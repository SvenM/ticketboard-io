import { Component, OnInit } from '@angular/core';
import {cloneDeep} from 'lodash';

@Component({
  selector: 'jr-calendar',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage implements OnInit {

  public days: Date[];
  public today: Date;
  public firstOfMonth: Date;

  constructor() { }

  ngOnInit() {
    this.today = new Date();
    const date = new Date(this.today.getFullYear(), this.today.getMonth(), 1);
    this.setDate(date);
  }

  public onPrevious() {
    const date = new Date(this.firstOfMonth.getFullYear(), this.firstOfMonth.getMonth() - 1, 1);
    this.setDate(date);
  }

  public onNext() {
    const date = new Date(this.firstOfMonth.getFullYear(), this.firstOfMonth.getMonth() + 1, 1);
    this.setDate(date);
  }

  private setDate(date: Date) {
    this.firstOfMonth = cloneDeep(date);
    const month = date.getMonth();
    this.days = [];
    while (date.getMonth() === month) {
      this.days.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
  }

}
