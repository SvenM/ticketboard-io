import { Injectable } from '@angular/core';
import { ProfileService } from '../profile/profile.service';
import { Auth2Service } from '../auth/auth2.service';
import { switchMap, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import { Profile } from '../auth/profile.model';

@Injectable({
    providedIn: 'root'
})
export class BookingService {

    constructor(
        private authService: Auth2Service,
        private profileService: ProfileService,
        private router: Router
    ) {
    }

    public checkIfUserProfileIsComplete(eventId: string): Observable<any> {
        return this.authService.user.pipe(
            take(1),
            switchMap(user => {
                if (!(user && user.uid)) {
                    return from(this.router.navigateByUrl('/auth'));
                } else {
                    console.log('getProfile');
                    return this.profileService.getProfile(user.uid);
                }
            }),
            switchMap((profile: Profile) => {
                console.log('next');
                if (profile) {
                    if (
                        profile.firstName &&
                        profile.lastName &&
                        profile.address &&
                        profile.zip &&
                        profile.city &&
                        profile.country &&
                        profile.email &&
                        profile.phoneNumber &&
                        profile.birthday
                    ) {
                        return this.bookEvent(eventId);
                    } else {
                        // redirect to setting profile to be ready for booking
                        return from(this.router.navigateByUrl('/core/profile/complete-profile/' + profile.id));
                    }
                }
            })
        );
    }

    public bookEvent(eventId: string): Observable<any> {
        // from here logic to book event
        console.log('bookEvent');
        return from(this.router.navigateByUrl('/booking/' + eventId));
    }
}
