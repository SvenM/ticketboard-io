import { Component, OnInit } from '@angular/core';
import { Event } from '../../core/events/event.model';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '../../core/events/event.service';
import { AlertController, NavController } from '@ionic/angular';
import { DetailPageComponent } from '../../shared/components/classes/detail-page.component';

@Component({
    selector: 'tb-book-event',
    templateUrl: './book-event.page.html',
    styleUrls: ['./book-event.page.scss'],
})
export class BookEventPage extends DetailPageComponent implements OnInit {

    public isLoading = false;
    public event: Partial<Event>;

    constructor(
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        private route: ActivatedRoute,
        private eventService: EventService,
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            this.navigateBack();
            return;
        }
        this.isLoading = true;
        const event: Event = await this.eventService.getEvent(id).catch(() => {
            this.showAlert({message: 'Request details could not be fetched. Please try again later.'});
            return null;
        });
        if (!event) {
            await this.showAlert({message: 'Request details could not be fetched. Please try again later.'});

            return;
        }
        this.event = event;
        this.isLoading = false;
    }
}
