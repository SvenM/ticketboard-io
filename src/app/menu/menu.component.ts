import { Component, OnInit } from '@angular/core';
import { Auth2Service } from '../auth/auth2.service';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BusinessAuthService } from '../business/business-auth/business-auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  public isLoggedIn$: Observable<boolean>;
  public isBusinessLoggedIn$: Observable<boolean>;
  public isBusiness$: Observable<boolean>;

  constructor(private authService: Auth2Service, private businessAuthService: BusinessAuthService, private router: Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.user.pipe(map(user => !!user));
    this.isBusinessLoggedIn$ = this.businessAuthService.businessUser.pipe(map(user => !!user));
    this.isBusiness$ = this.businessAuthService.isBusinessUser();
  }

  onAuthClick() {
    this.isLoggedIn$.pipe(take(1)).subscribe(isAuth => {
      if (isAuth) {
        this.authService.logout().then(() => {
          this.router.navigateByUrl('/core');
        });
      } else {
        this.router.navigate(['auth']);
      }
    });
  }

  onBusinessAuthClicked() {
    this.isBusinessLoggedIn$.pipe(take(1)).subscribe(isAuth => {
      if (isAuth) {
        this.businessAuthService.logout().then(() => {
          this.router.navigateByUrl('/core');
        });
      } else {
        this.router.navigate(['business/business-auth']);
      }
    });
  }
}
