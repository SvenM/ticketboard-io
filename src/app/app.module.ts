import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { eventsFilterReducer, EventsFilterState } from './state-management/eventsFilter/events-filter.reducer';
import { SharedModule } from './shared/shared.module';
import { tracksFilterReducer, TracksFilterState } from './state-management/tracksFilter/tracks-filter.reducer';
import { BusinessPageModule } from './business/business.module';
import { tracksReducer, TracksState } from './state-management/tracks/tracks.reducer';
import { TracksEffects } from './state-management/tracks/tracks.effects';
import { promoterReducer, PromotersState } from './state-management/promoters/promoters.reducer';
import { PromotersEffects } from './state-management/promoters/promoters.effects';
import { eventReducer, EventsState } from './state-management/events/events.reducer';
import { EventsEffects } from './state-management/events/events.effects';
import { CalendarPageModule } from './calendar/calendar.module';

export interface AppState {
    eventsFilter: EventsFilterState;
    tracks: TracksState;
    tracksFilter: TracksFilterState;
    promoters: PromotersState;
    events: EventsState;
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireAuthModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        AngularFireAuthGuardModule,
        StoreModule.forRoot({
            eventsFilter: eventsFilterReducer,
            tracks: tracksReducer,
            tracksFilter: tracksFilterReducer,
            promoters: promoterReducer,
            events: eventReducer,
        }),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production
        }),
        EffectsModule.forRoot([
            TracksEffects,
            PromotersEffects,
            EventsEffects,
        ]),
        SharedModule,
        BusinessPageModule,
        CalendarPageModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
