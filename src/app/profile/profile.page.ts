import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { ProfileService } from './profile.service';
import { Profile } from '../auth/profile.model';
import { FormControl, FormGroup } from '@angular/forms';
import { Auth2Service } from '../auth/auth2.service';
import { User } from 'firebase';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { DetailPageComponent } from '../shared/components/classes/detail-page.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends DetailPageComponent implements OnInit {

    public profile$: Observable<Partial<Profile>>;

    public form: FormGroup;
    public isLoading = false;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private auth2Service: Auth2Service,
        private profileService: ProfileService,
        private toastCtrl: ToastController
    ) {
        super(navCtrl, alertCtrl);
    }

    public ngOnInit() {
        this.profile$ = this.auth2Service.user.pipe(
            switchMap((user: User) => this.profileService.getProfile(user?.uid)),
        );

        this.initialize();
    }

    public ionViewWillEnter() {
        this.initialize();
    }

    private initialize() {
        this.isLoading = true;

        this.auth2Service.user.pipe(
            switchMap((user: User) => this.profileService.getProfile(user?.uid)),
            take(1)
        ).subscribe(async (profile: Profile) => {
            if (!profile) {
                await this.showAlert({message: 'Profile data could not be fetched. Please try again later.'});
            }

            this.form = new FormGroup({
                birthday: new FormControl(
                    profile.privacySettings && profile.privacySettings.birthday ? profile.privacySettings.birthday : null, {
                        updateOn: 'change'
                    }),
                email: new FormControl(
                    profile.privacySettings && profile.privacySettings.email ? profile.privacySettings.email : null, {
                        updateOn: 'change'
                    }),
                location: new FormControl(
                    profile.privacySettings && profile.privacySettings.location ? profile.privacySettings.location : null, {
                        updateOn: 'change'
                    }),
                phoneNumber: new FormControl(
                    profile.privacySettings && profile.privacySettings.phoneNumber ? profile.privacySettings.phoneNumber : null, {
                        updateOn: 'change'
                    }),
                facebook: new FormControl(
                    profile.privacySettings && profile.privacySettings.facebook ? profile.privacySettings.facebook : null, {
                        updateOn: 'change'
                    }),
                instagram: new FormControl(
                    profile.privacySettings && profile.privacySettings.instagram ? profile.privacySettings.instagram : null, {
                        updateOn: 'change'
                    }),
                twitter: new FormControl(
                    profile.privacySettings && profile.privacySettings.twitter ? profile.privacySettings.twitter : null, {
                        updateOn: 'change'
                    }),
                youtube: new FormControl(
                    profile.privacySettings && profile.privacySettings.youtube ? profile.privacySettings.youtube : null, {
                        updateOn: 'change'
                    }),
            });
            this.isLoading = false;
        });
    }

    public onSaveSettings() {
        this.profile$.pipe(
            take(1)
        ).subscribe(async profile => {
            await this.profileService.updatePrivacySettings(profile.id, {
                birthday: this.form.value.birthday,
                email: this.form.value.email,
                location: this.form.value.location,
                phoneNumber: this.form.value.phoneNumber,
                facebook: this.form.value.facebook,
                instagram: this.form.value.instagram,
                twitter: this.form.value.twitter,
                youtube: this.form.value.youtube
            });
            const toast = await this.toastCtrl.create({
                message: '<b>Your settings have been saved.</b>',
                duration: 2000,
                animated: true,
                color: 'secondary'
            });
            await toast.present();
        });
    }
}
