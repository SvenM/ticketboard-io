import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { Subscription, from } from 'rxjs';

import { ProfileService } from '../profile.service';
import { UploadImageService } from 'src/app/shared/upload-image.service';
import { SharedService } from 'src/app/shared/shared.service';
import { ActivatedRoute } from '@angular/router';
import { take, switchMap, catchError } from 'rxjs/operators';
import { Profile } from 'src/app/auth/profile.model';
import { ChooseTrackPage } from '../../shared/modals/choose-track/choose-track.page';
import { DetailPageComponent } from '../../shared/components/classes/detail-page.component';


@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage extends DetailPageComponent implements OnInit {

    form: FormGroup;

    isLoading = false;
    profileSub: Subscription;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private profileService: ProfileService,
        private loaderCtrl: LoadingController,
        private uploadImageService: UploadImageService,
        private sharedService: SharedService,
        private modalCtrl: ModalController
    ) {
        super(navCtrl, alertCtrl);
    }

    public ngOnInit() {
        this.initialize();
    }

    public ionViewWillEnter() {
        this.initialize();
    }

    public async onSelectTrack() {
        const modal = await this.modalCtrl.create({
            component: ChooseTrackPage,
            componentProps: {
                track: this.form.value.favoriteTrack
            }
        });
        await modal.present();
        const {data} = await modal.onWillDismiss();
        if (data) {
            this.form.patchValue({favoriteTrack: data});
        }
    }

    private initialize() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            this.navigateBack();
            return;
        }
        this.isLoading = true;
        this.profileSub = this.profileService.getProfile(id).subscribe(
            async (profile: Profile) => {
                if (!profile) {
                    await this.showAlert({message: 'Profile could not be fetched. Please try again later.'});
                }

                this.form = new FormGroup({
                    profileImgUrl: new FormControl(profile.profileImgUrl ? profile.profileImgUrl : null),
                    nickname: new FormControl(profile.nickname ? profile.nickname : null, {
                        updateOn: 'change'
                    }),
                    bike: new FormControl(profile.bike ? profile.bike : null, {
                        updateOn: 'change'
                    }),
                    favoriteTrack: new FormControl(profile.favoriteTrack ? profile.favoriteTrack : null, {
                        updateOn: 'change'
                    }),
                    email: new FormControl(profile.email ? profile.email : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    firstName: new FormControl(profile.firstName ? profile.firstName : null, {
                        updateOn: 'change'
                    }),
                    lastName: new FormControl(profile.lastName ? profile.lastName : null, {
                        updateOn: 'change'
                    }),
                    birthday: new FormControl(
                        profile.birthday ? new Date(profile.birthday).toUTCString() : null,
                        {
                            updateOn: 'change'
                        }
                    ),
                    country: new FormControl(profile.country ? profile.country : null, {
                        updateOn: 'change'
                    }),
                    zip: new FormControl(profile.zip ? profile.zip : null, {
                        updateOn: 'change'
                    }),
                    city: new FormControl(profile.city ? profile.city : null, {
                        updateOn: 'change'
                    }),
                    address: new FormControl(profile.address ? profile.address : null, {
                        updateOn: 'change'
                    }),
                    phoneNumber: new FormControl(profile.phoneNumber ? profile.phoneNumber : null, {
                        updateOn: 'change'
                    }),
                    facebook: new FormControl(profile.facebook ? profile.facebook : null, {
                        updateOn: 'change'
                    }),
                    instagram: new FormControl(profile.instagram ? profile.instagram : null, {
                        updateOn: 'change'
                    }),
                    twitter: new FormControl(profile.twitter ? profile.twitter : null, {
                        updateOn: 'change'
                    }),
                    youtube: new FormControl(profile.youtube ? profile.youtube : null, {
                        updateOn: 'change'
                    })
                });

                this.isLoading = false;
            }, error => this.showAlert({message: 'Profile could not be fetched. Please try again later.'})
        );

    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'profileImgUrl');
    }

    public async onUpdateProfile() {

        const id = this.route.snapshot.params.id;

        const loadingEl = await this.loaderCtrl.create({
            message: 'Updating profile...'
        });

        await loadingEl.present();

        await this.profileService.updateProfile(id, {
            nickname: this.form.value.nickname,
            bike: this.form.value.bike,
            favoriteTrack: this.form.value.favoriteTrack,
            email: this.form.value.email,
            firstName: this.form.value.firstName,
            lastName: this.form.value.lastName,
            birthday: this.form.value.birthday ? new Date(this.form.value.birthday) : null,
            country: this.form.value.country,
            zip: this.form.value.zip,
            city: this.form.value.city,
            address: this.form.value.address,
            phoneNumber: this.form.value.phoneNumber,
            profileImgUrl: this.form.value.profileImgUrl,
            facebook: this.form.value.facebook,
            instagram: this.form.value.instagram,
            twitter: this.form.value.twitter,
            youtube: this.form.value.youtube
        });

        await loadingEl.dismiss();
        this.navigateBack();
    }
}
