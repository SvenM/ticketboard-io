import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { take, map, switchMap,  } from 'rxjs/operators';

import { Profile } from '../auth/profile.model';
import { PrivacySettings } from '../auth/privacy-settings.model';
import { Auth2Service } from '../auth/auth2.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private USERS_COLLECTION_NAME = 'users';

  constructor(private db: AngularFirestore, private authService: Auth2Service) {
  }

  public changeNickname(profileId: string, nickname: string): Promise<void> {
    return this.db.collection(this.USERS_COLLECTION_NAME).doc(profileId).update({nickname});
  }

  public deleteProfile(profileId: string): Promise<any> {
    return this.db.collection(this.USERS_COLLECTION_NAME).doc(profileId).delete();
  }

  public getProfile(profileId: string): Observable<Partial<Profile>> {
    if(!profileId) {
      return of(null);
    }
    return this.db.collection(this.USERS_COLLECTION_NAME).doc(profileId).get().pipe(
      take(1),
      map(profile => {
        return {
          id: profileId,
          ...profile.data(),
          birthday: profile.data().birthday ? (profile.data().birthday as any).toDate() : null
        };
      })
    );
  }

  public getUnreadMessagesAmount(): Observable<number> {
    return this.authService.user.pipe(switchMap((user: firebase.User) => {
      return this.db
          .collection(this.USERS_COLLECTION_NAME)
          .doc(user.uid)
          .valueChanges()
          .pipe(map((profile: Profile) => profile.unreadMessages))
    }))
  }

  public updatePrivacySettings(profileId: string, privacySettings: PrivacySettings) {
      return this.db.collection(this.USERS_COLLECTION_NAME).doc(profileId).update({privacySettings})
  }

  public updateProfile(profileId: string, data: Partial<Profile>): Promise<any> {
    return this.db.collection(this.USERS_COLLECTION_NAME).doc(profileId).update(data);
  }
}
