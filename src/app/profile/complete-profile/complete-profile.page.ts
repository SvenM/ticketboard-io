import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from '../profile.service';
import { AlertController, LoadingController, ModalController, NavController } from '@ionic/angular';
import { UploadImageService } from '../../shared/upload-image.service';
import { SharedService } from '../../shared/shared.service';
import { ChooseTrackPage } from '../../shared/modals/choose-track/choose-track.page';
import { Profile } from '../../auth/profile.model';
import { DetailPageComponent } from '../../shared/components/classes/detail-page.component';

@Component({
    selector: 'tb-complete-profile',
    templateUrl: './complete-profile.page.html',
    styleUrls: ['./complete-profile.page.scss'],
})
export class CompleteProfilePage extends DetailPageComponent implements OnInit {

    form: FormGroup;

    isLoading = false;
    profileSub: Subscription;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private profileService: ProfileService,
        private loaderCtrl: LoadingController,
        private uploadImageService: UploadImageService,
        private sharedService: SharedService,
        private modalCtrl: ModalController
    ) {
        super(navCtrl, alertCtrl);
    }

    public ngOnInit() {
        this.initialize();
    }

    public ionViewWillEnter() {
        this.initialize();
    }

    public async onSelectTrack() {
        const modal = await this.modalCtrl.create({
            component: ChooseTrackPage,
            componentProps: {
                track: this.form.value.favoriteTrack
            }
        });
        await modal.present();
        const {data} = await modal.onWillDismiss();
        if (data) {
            this.form.patchValue({favoriteTrack: data});
        }
    }

    private initialize() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            this.navigateBack();
            return;
        }
        this.isLoading = true;
        this.profileSub = this.profileService.getProfile(id).subscribe(
            async (profile: Profile) => {
                if (!profile) {
                    await this.showAlert({message: 'Profile could not be fetched. Please try again later.'});
                }

                this.form = new FormGroup({
                    email: new FormControl(profile.email ? profile.email : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    firstName: new FormControl(profile.firstName ? profile.firstName : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    lastName: new FormControl(profile.lastName ? profile.lastName : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    birthday: new FormControl(
                        profile.birthday ? new Date(profile.birthday).toUTCString() : null,
                        {
                            updateOn: 'change',
                            validators: [Validators.required]
                        }
                    ),
                    country: new FormControl(profile.country ? profile.country : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    zip: new FormControl(profile.zip ? profile.zip : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    city: new FormControl(profile.city ? profile.city : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    address: new FormControl(profile.address ? profile.address : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    phoneNumber: new FormControl(profile.phoneNumber ? profile.phoneNumber : null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                });

                this.isLoading = false;
            }, error => this.showAlert({message: 'Profile could not be fetched. Please try again later.'})
        );

    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'profileImgUrl');
    }

    public async onUpdateProfile() {
        const id = this.route.snapshot.params.id;

        const loadingEl = await this.loaderCtrl.create({
            message: 'Updating profile...'
        });

        await loadingEl.present();

        await this.profileService.updateProfile(id, {
            email: this.form.value.email,
            firstName: this.form.value.firstName,
            lastName: this.form.value.lastName,
            birthday: this.form.value.birthday ? new Date(this.form.value.birthday) : null,
            country: this.form.value.country,
            zip: this.form.value.zip,
            city: this.form.value.city,
            address: this.form.value.address,
            phoneNumber: this.form.value.phoneNumber
        });

        await loadingEl.dismiss();
        this.navigateBack();
    }
}
