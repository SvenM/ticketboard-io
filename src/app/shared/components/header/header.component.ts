import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'jr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() public title: string;
  @Input() public backUrl: string;

  constructor(

  ) { }

  public ngOnInit() {
  }
}
