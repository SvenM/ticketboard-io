import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'tb-infinite-scroll-content',
  templateUrl: './infinite-scroll-content.component.html',
  styleUrls: ['./infinite-scroll-content.component.scss'],
})
export class InfiniteScrollContentComponent implements OnInit, OnDestroy {

  @ViewChild('content') public content;
  @Input() public infiniteService: any;

  public showScrollToTopButton = false;

  private unsubscribe: Subject<void>;


  constructor() { }

  ngOnInit() {
    this.unsubscribe = new Subject();
  }

  public ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public onScrollToTop() {
    this.content.scrollToTop(500);
  }

  public watchScroll(event) {
    if(event.detail) {
      this.showScrollToTopButton = event.detail.scrollTop > 500;
    }
  }

  public onScrollBottom(event) {
    this.infiniteService.more();
    this.infiniteService.loading.pipe(takeUntil(this.unsubscribe)).subscribe(loading => {
          if (!loading) {
            event.target.complete();
            this.unsubscribe.next();
          }
        }
    )
  }
}
