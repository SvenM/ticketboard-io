import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'jr-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./menu-header.component.scss'],
})
export class MenuHeaderComponent implements OnInit {

  @Input() public title: string;

  public bikeTypeColor$: Observable<string>;

  constructor(
  ) { }

  public ngOnInit() {
  }
}
