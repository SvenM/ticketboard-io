import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'jr-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
})
export class SpinnerComponent {
  @Input() public text: string;
}
