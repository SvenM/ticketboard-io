import { Component } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

@Component({
    selector: 'detailPage',
    template: '<div>DetailPage</div>'
})
export class DetailPageComponent {
    constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    }

    protected async showAlert({
                                  header = 'An error occured!',
                                  message = 'Details could not be fetched. Please try again later.'
                              }: { header?: string, message?: string }) {
        const alertEl = await this.alertCtrl.create({
            header, message, buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        this.navigateBack();
                    }
                }
            ]
        });
        await alertEl.present();

    }

    protected async navigateBack() {
        console.log(this.navCtrl);
        await this.navCtrl.pop();
    }
}
