import { Pipe, PipeTransform } from '@angular/core';
import { FilterSelectObject, TrainingTypeFilterSelect } from '../models/filter-select.model';

@Pipe({
  name: 'eventType'
})
export class EventTypePipe implements PipeTransform {

  eventTypes = TrainingTypeFilterSelect;

  transform(eventTypeCode: string): string {


    const eventType: FilterSelectObject = this.eventTypes.find(e => eventTypeCode === e.key);

    if(eventType) {
      return eventType.selectorText;
    }

    return null;
  }
}
