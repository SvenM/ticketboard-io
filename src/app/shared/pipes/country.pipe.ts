import { Pipe, PipeTransform } from '@angular/core';
import { CountryFilterSelect, FilterSelectObject } from '../models/filter-select.model';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  countries = CountryFilterSelect;

  transform(countryCode: string): string {

    const country: FilterSelectObject = this.countries.find(c => countryCode === c.key);

    if(country) {
      return country.selectorText;
    }

    return countryCode;
  }

}
