import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { scan, take, tap } from 'rxjs/operators';

export interface QueryConfig {
    path: string;
    field: string;
    limit?: number;
}

@Injectable({
    providedIn: 'root'
})
export class PaginationService {

    // tslint:disable-next-line:variable-name
    private _done = new BehaviorSubject(false);
    // tslint:disable-next-line:variable-name
    private _loading = new BehaviorSubject(false);
    // tslint:disable-next-line:variable-name
    private _data = new BehaviorSubject([]);

    private query: QueryConfig;

    data: Observable<any>;
    done: Observable<boolean> = this._done.asObservable();
    loading: Observable<boolean> = this._loading.asObservable();

    constructor(private db: AngularFirestore) {
    }

    init(path: string, field: string, opts?: any) {
        this.query = {
            path,
            field,
            limit: 10
        };

        const first = this.db.collection(this.query.path, ref => {
            return ref.orderBy(this.query.field).limit(this.query.limit);
        });

        this.mapAndUpdate(first);

        this.data = this._data.asObservable().pipe(scan((acc, val) => {
            return acc.concat(val);
        }));
    }

    more() {
        const cursor = this.getCursor();

        const more = this.db.collection(this.query.path, ref => {
            return ref.orderBy(this.query.field)
                .limit(this.query.limit)
                .startAfter(cursor);
        });
        this.mapAndUpdate(more);
    }

    private getCursor() {
        const current = this._data.value;
        if (current.length) {
            return current[current.length - 1].doc;
        }
        return null;
    }

    private mapAndUpdate(col: AngularFirestoreCollection<any>) {
        if (this._done.value || this._loading.value) {
            return;
        }

        // loading
        this._loading.next(true);

        return col.snapshotChanges()
            .pipe(
                tap(arr => {

                    // Make this block injectable
                    const values = arr.map(snap => {
                        const data = snap.payload.doc.data();
                        const doc = snap.payload.doc;
                        return {...data, doc};
                    });

                    this._data.next(values);
                    this._loading.next(false);

                    if (!values.length) {
                        this._done.next(true);
                    }
                }),
                take(1)
            ).subscribe();
    }
}
