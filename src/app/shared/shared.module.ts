import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ImagePickerComponent } from './pickers/image-picker/image-picker.component';
import { JsonPickerComponent } from './pickers/json-picker/json-picker.component';
import { CountryPipe } from './pipes/country.pipe';
import { EventTypePipe } from './pipes/event-type.pipe';
import { PromoterSelectComponent } from './forms/promoter.select/promoter.select.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TracksSelectComponent } from './forms/tracks.select/tracks.select.component';
import { CountrySelectComponent } from './forms/country.select/country.select.component';
import { TextInputComponent } from './forms/text-input/text-input.component';
import { NumberInputComponent } from './forms/number-input/number-input.component';
import { EmailInputComponent } from './forms/email-input/email-input.component';
import { ToggleComponent } from './forms/toggle/toggle.component';
import { DateInputComponent } from './forms/date-input/date-input.component';
import { ChooseEventPageModule } from './modals/choose-event/choose-event.module';
import { ChooseTrackPageModule } from './modals/choose-track/choose-track.module';
import { InfiniteScrollContentComponent } from './components/infinite-scroll-content/infinite-scroll-content.component';
import { MenuComponent } from '../menu/menu.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { MenuHeaderComponent } from './components/menu-header-component/menu-header.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { DetailPageComponent } from './components/classes/detail-page.component';

@NgModule({
    imports: [CommonModule, IonicModule, ReactiveFormsModule, ChooseEventPageModule, ChooseTrackPageModule, RouterModule],
    declarations: [
        ImagePickerComponent,
        JsonPickerComponent,
        CountryPipe,
        EventTypePipe,
        PromoterSelectComponent,
        TracksSelectComponent,
        CountrySelectComponent,
        TextInputComponent,
        NumberInputComponent,
        EmailInputComponent,
        ToggleComponent,
        DateInputComponent,
        InfiniteScrollContentComponent,
        MenuComponent,
        HeaderComponent,
        MenuHeaderComponent,
        SpinnerComponent,
        DetailPageComponent
    ],
    exports: [
        ImagePickerComponent,
        JsonPickerComponent,
        CountryPipe, EventTypePipe,
        PromoterSelectComponent,
        TracksSelectComponent,
        CountrySelectComponent,
        TextInputComponent,
        NumberInputComponent,
        EmailInputComponent,
        ToggleComponent,
        DateInputComponent,
        InfiniteScrollContentComponent,
        MenuComponent,
        HeaderComponent,
        MenuHeaderComponent,
        SpinnerComponent,
        DetailPageComponent
    ]
})
export class SharedModule {

}
