import { Injectable } from '@angular/core';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable, from, of } from 'rxjs';
import { switchMap, map, delay } from 'rxjs/operators';
import { uuid } from 'uuidv4';


@Injectable({
    providedIn: 'root'
})
export class UploadImageService {

    // Main task
    private task: AngularFireUploadTask;

    // Download URL
    downloadURL: Observable<string>;

    constructor(private storage: AngularFireStorage) {
    }

    public startUpload(imageFile: any): Observable<string> {
        const path = 'images/' + uuid();

        this.task = this.storage.upload(path, imageFile);

        return this.task.snapshotChanges().pipe(
            map(snap => {
                return (snap.bytesTransferred / snap.totalBytes === 1.0) ? snap.ref : null;
            }),
            delay(1000),
            switchMap(ref => {
                return ref ? from(ref.getDownloadURL()) : of(null);
            })
        );

    }


}
