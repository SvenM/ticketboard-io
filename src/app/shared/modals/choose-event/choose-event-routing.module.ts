import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseEventPage } from './choose-event.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseEventPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseEventPageRoutingModule {}
