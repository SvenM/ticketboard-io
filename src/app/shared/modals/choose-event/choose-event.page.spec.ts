import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseEventPage } from './choose-event.page';

describe('ChooseEventPage', () => {
  let component: ChooseEventPage;
  let fixture: ComponentFixture<ChooseEventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseEventPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseEventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
