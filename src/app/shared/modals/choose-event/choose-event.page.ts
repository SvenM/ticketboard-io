import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EventService } from '../../../core/events/event.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import _ from 'lodash';

@Component({
  selector: 'tb-choose-event',
  templateUrl: './choose-event.page.html',
  styleUrls: ['./choose-event.page.scss'],
})
export class ChooseEventPage implements OnInit {

  constructor(private modalCtrl: ModalController, public events$: EventService) { }

  @ViewChild('content') content;

  public form: FormGroup;
  public showScrollToTopButton = false;

  @Input() event: Partial<Event>;

  ngOnInit() {
    this.form = new FormGroup({
      event: new FormControl(this.event, {
        updateOn: 'change',
        validators: [Validators.required]
      })
    });

    this.events$.init();
  }

  public onSelect() {
    this.modalCtrl.dismiss(_.omit(this.form.value.event, ['doc']));
  }

  public onDismissModal() {
    this.modalCtrl.dismiss();
  }

  public onScrollToTop() {
    this.content.scrollToTop(500);
  }

  public watchScroll(event) {
    if(event.detail) {
      this.showScrollToTopButton = event.detail.scrollTop > 500;
    }
  }

  public onScrollBottom(event) {
    this.events$.more();
    const sub = this.events$.loading.subscribe(loading => {
          if (!loading) {
            event.target.complete();
            sub.unsubscribe();
          }
        }
    )
  }

}
