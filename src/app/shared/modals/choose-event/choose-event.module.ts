import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseEventPageRoutingModule } from './choose-event-routing.module';

import { ChooseEventPage } from './choose-event.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChooseEventPageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [ChooseEventPage]
})
export class ChooseEventPageModule {
}
