import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseTrackPage } from './choose-track.page';

describe('ChooseTrackPage', () => {
  let component: ChooseTrackPage;
  let fixture: ComponentFixture<ChooseTrackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseTrackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseTrackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
