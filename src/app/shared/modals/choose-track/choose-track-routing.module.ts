import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseTrackPage } from './choose-track.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseTrackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseTrackPageRoutingModule {}
