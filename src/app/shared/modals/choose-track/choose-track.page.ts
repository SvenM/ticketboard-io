import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TrackService } from '../../../core/tracks/track.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'tb-choose-track',
    templateUrl: './choose-track.page.html',
    styleUrls: ['./choose-track.page.scss'],
})
export class ChooseTrackPage implements OnInit {

    @ViewChild('content') content;

    @Input() track: string;

    public form: FormGroup;
    public showScrollToTopButton = false;

    constructor(private modalCtrl: ModalController, public tracks$: TrackService) {
    }

    ngOnInit() {
        this.form = new FormGroup({
            track: new FormControl(this.track, {
                updateOn: 'change',
                validators: [Validators.required]
            })
        });

        this.tracks$.init();
    }

    public onSelect() {
        this.modalCtrl.dismiss(this.form.value.track);
    }

    public onDismissModal() {
        this.modalCtrl.dismiss();
    }

    public onScrollToTop() {
        this.content.scrollToTop(500);
    }

    public watchScroll(event) {
        if (event.detail) {
            this.showScrollToTopButton = event.detail.scrollTop > 500;
        }
    }

    public onScrollBottom(event) {
        this.tracks$.more();
        const sub = this.tracks$.loading.subscribe(loading => {
                if (!loading) {
                    event.target.complete();
                    sub.unsubscribe();
                }
            }
        )
    }
}
