import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseTrackPageRoutingModule } from './choose-track-routing.module';

import { ChooseTrackPage } from './choose-track.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseTrackPageRoutingModule,
      ReactiveFormsModule
  ],
  declarations: [ChooseTrackPage]
})
export class ChooseTrackPageModule {}
