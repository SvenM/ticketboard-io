import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'tb-email-input',
  templateUrl: './email-input.component.html',
  styleUrls: ['./email-input.component.scss'],
})
export class EmailInputComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() parentFormControlName: string;
  @Input() label: string;

  constructor() { }

  ngOnInit() {}

}
