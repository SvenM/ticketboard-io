import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'tb-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss'],
})
export class DateInputComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() parentFormControlName: string;
  @Input() label: string;
  @Input() min: string;
  @Input() max: string;

  constructor() { }

  ngOnInit() {}

}
