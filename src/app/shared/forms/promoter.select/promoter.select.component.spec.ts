import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Promoter.SelectComponent } from './promoter.select.component';

describe('Promoter.SelectComponent', () => {
  let component: Promoter.SelectComponent;
  let fixture: ComponentFixture<Promoter.SelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Promoter.SelectComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Promoter.SelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
