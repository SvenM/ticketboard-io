import { Component, Input, OnInit } from '@angular/core';
import { PromoterService } from '../../../core/promoters/promoter.service';
import { Observable } from 'rxjs';
import { Promoter } from '../../../core/promoters/promoter.model';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'tb-promoter-select',
    templateUrl: './promoter.select.component.html',
    styleUrls: ['./promoter.select.component.scss'],
})
export class PromoterSelectComponent implements OnInit {

    @Input() parentForm: FormGroup;

    public promoters$: Observable<Partial<Promoter>[]>;

    public selectPromoterAlertOptions: any = {
        header: 'Select a Promoter',
        translucent: true
    };

    constructor(
        private promoterService: PromoterService,
    ) {
    }

    public ngOnInit() {
        this.promoters$ = this.promoterService.getAllPromoters();
    }


}
