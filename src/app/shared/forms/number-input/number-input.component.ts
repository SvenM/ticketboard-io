import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'tb-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.scss'],
})
export class NumberInputComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() parentFormControlName: string;
  @Input() label: string;

  constructor() { }

  ngOnInit() {}

}
