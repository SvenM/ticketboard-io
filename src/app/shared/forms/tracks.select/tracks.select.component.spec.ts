import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Tracks.SelectComponent } from './tracks.select.component';

describe('Tracks.SelectComponent', () => {
  let component: Tracks.SelectComponent;
  let fixture: ComponentFixture<Tracks.SelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tracks.SelectComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Tracks.SelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
