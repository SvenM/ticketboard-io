import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TrackService } from '../../../core/tracks/track.service';
import { Observable } from 'rxjs';
import { Track } from '../../../core/tracks/track.model';

@Component({
    selector: 'tb-tracks-select',
    templateUrl: './tracks.select.component.html',
    styleUrls: ['./tracks.select.component.scss'],
})
export class TracksSelectComponent implements OnInit {

    @Input() parentForm: FormGroup;
    @Input() parentFormControlName = 'track';
    @Input() label = 'track';

    public tracks$: Observable<Partial<Track>[]>;

    public selectTrackAlertOptions: any = {
        header: 'Select a Track',
        translucent: true
    };

    constructor(
        private trackService: TrackService,
    ) {
    }

    public ngOnInit() {
        this.tracks$ = this.trackService.getAllTracks();
    }

}
