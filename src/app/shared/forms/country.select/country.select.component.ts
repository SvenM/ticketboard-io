import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CountryFilterSelect, FilterSelectObject } from '../../models/filter-select.model';

@Component({
  selector: 'tb-country-select',
  templateUrl: './country.select.component.html',
  styleUrls: ['./country.select.component.scss'],
})
export class CountrySelectComponent implements OnInit {

  @Input() parentForm: FormGroup;

  countries: FilterSelectObject[] = CountryFilterSelect;

  public selectCountryAlertOptions: any = {
    header: 'Select a country',
    translucent: true
  };

  constructor() { }

  ngOnInit() {}

}
