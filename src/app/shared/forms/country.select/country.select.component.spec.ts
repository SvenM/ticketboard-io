import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Country.SelectComponent } from './country.select.component';

describe('Country.SelectComponent', () => {
  let component: Country.SelectComponent;
  let fixture: ComponentFixture<Country.SelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Country.SelectComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Country.SelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
