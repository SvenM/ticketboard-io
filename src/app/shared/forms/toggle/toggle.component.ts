import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'tb-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
})
export class ToggleComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() parentFormControlName: string;
  @Input() label: string;

  constructor() { }

  ngOnInit() {}

}
