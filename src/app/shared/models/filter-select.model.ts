import { BikeType, TrainingType } from '../../state-management/eventsFilter/events-filter.reducer';

export interface FilterSelectObject {
    key: string;
    selectorText: string;
}

export const BikeTypeFilterSelect: FilterSelectObject[] = [
    {
        key: BikeType.BIKE,
        selectorText: 'Bike'
    },
    {
        key: BikeType.SUPERMOTO,
        selectorText: 'Supermoto'
    },
    {
        key: BikeType.PITBIKE,
        selectorText: 'Pitbike'
    },
    {
        key: BikeType.ROLLER,
        selectorText: 'Roller'
    },
    {
        key: BikeType.ENDURO,
        selectorText: 'Enduro'
    }
];

export const TrainingTypeFilterSelect: FilterSelectObject[] = [
    {
        key: TrainingType.RACE,
        selectorText: 'Race'
    },
    {
        key: TrainingType.SECURITY,
        selectorText: 'Security'
    },
    {
        key: TrainingType.MOTO_GP,
        selectorText: 'MotoGP'
    },
    {
        key: TrainingType.WSBK,
        selectorText: 'WSBK'
    },
    {
        key: TrainingType.IDM,
        selectorText: 'IDM'
    },
    {
        key: TrainingType.DLC,
        selectorText: 'DLC'
    },
    {
        key: TrainingType.RLC,
        selectorText: 'RLC'
    },
    {
        key: TrainingType.ALPE_ADRIA,
        selectorText: 'AlpeAdria'
    },
    {
        key: TrainingType.PRO_STK,
        selectorText: 'ProStk'
    }
];

export enum CountryCodes {
    AR = 'AR',
    AT = 'AT',
    BE = 'BE',
    CZ = 'CZ',
    DE = 'DE',
    DK = 'DK',
    ES = 'ES',
    FI = 'FI',
    FR = 'FR',
    GB = 'GB',
    HR = 'HR',
    HU = 'HU',
    IT = 'IT',
    NL = 'NL',
    PL = 'PL',
    PT = 'PT',
    SE = 'SE',
    SK = 'SK',
    UK = 'UK',
    US = 'US'
}

export const CountryFilterSelect: FilterSelectObject[] = [
    {
        key: CountryCodes.AR,
        selectorText: 'Argentinien'
    },
    {
        key: CountryCodes.AT,
        selectorText: 'Östereich'
    },
    {
        key: CountryCodes.BE,
        selectorText: 'Belgien'
    },
    {
        key: CountryCodes.CZ,
        selectorText: 'Tscheschiche Republik'
    },
    {
        key: CountryCodes.DE,
        selectorText: 'Deutschland'
    },
    {
        key: CountryCodes.DK,
        selectorText: 'Dänemark'
    },
    {
        key: CountryCodes.ES,
        selectorText: 'Spanien'
    },
    {
        key: CountryCodes.FI,
        selectorText: 'Finnland'
    },
    {
        key: CountryCodes.FR,
        selectorText: 'Frankreich'
    },
    {
        key: CountryCodes.GB,
        selectorText: 'Großbritannien'
    },
    {
        key: CountryCodes.HR,
        selectorText: 'Kroatien'
    },
    {
        key: CountryCodes.HU,
        selectorText: 'Ungarn',
    },
    {
        key: CountryCodes.IT,
        selectorText: 'Italien'
    },
    {
        key: CountryCodes.NL,
        selectorText: 'Niederlande'
    },
    {
        key: CountryCodes.PL,
        selectorText: 'Polen'
    },
    {
        key: CountryCodes.PT,
        selectorText: 'Portugal'
    },
    {
        key: CountryCodes.SE,
        selectorText: 'Schweden'
    },
    {
        key: CountryCodes.SK,
        selectorText: 'Slowakei'
    },
    {
        key: CountryCodes.UK,
        selectorText: 'Vereinigtes Königreich'
    },
    {
        key: CountryCodes.US,
        selectorText: 'USA'
    },
];
