import { Injectable, OnDestroy } from '@angular/core';
import { of, Subject } from 'rxjs';
import { LoadingController, AlertController } from '@ionic/angular';
import { catchError, takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { UploadImageService } from './upload-image.service';

@Injectable({
    providedIn: 'root'
})
export class SharedService implements OnDestroy {

    private unsubscribe: Subject<void>;

    constructor(
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private uploadImageService: UploadImageService
    ) {
        this.unsubscribe = new Subject<void>();
    }

    public ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    public base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        const sliceSize = 1024;
        const byteCharacters = atob(base64Data);
        const bytesLength = byteCharacters.length;
        const slicesCount = Math.ceil(bytesLength / sliceSize);
        const byteArrays = new Array(slicesCount);

        for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            const begin = sliceIndex * sliceSize;
            const end = Math.min(begin + sliceSize, bytesLength);

            const bytes = new Array(end - begin);
            for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, {type: contentType});
    }

    public showSpinner(message: string = 'Lade...'): Promise<any> {
        return this.loadingCtrl.create({
            message
        });
    }

    public createDateFromGermanDateString(dateString: string): Date {
        const str = dateString.split('.');
        return new Date(+str[2], +str[1] - 1, +str[0]);
    }

    public createDateFrom(date: Date) {
        const newDate = new Date(date);
        newDate.setHours(0);
        newDate.setMinutes(0);
        newDate.setSeconds(0);
        newDate.setMilliseconds(0);
        return newDate;
    }

    public createDateTo(date: Date) {
        const newDate = new Date(date);
        newDate.setHours(23);
        newDate.setMinutes(59);
        newDate.setSeconds(59);
        newDate.setMilliseconds(999);
        return newDate;
    }

    public generateFirestoreId() {
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let autoId = '';
        for (let i = 0; i < 20; i++) {
            autoId += chars.charAt(Math.floor(Math.random() * chars.length));
        }

        return autoId;
    }

    public async onImagePicked(imageData: string | File, form: FormGroup, attributeName: string) {
        let imageFile;
        if (typeof imageData === 'string') {
            try {
                imageFile = this.base64toBlob(imageData.replace('data:image/jpeg;base64,', ''), 'image/jpeg');
            } catch (err) {
                const alertEl = await this.alertCtrl.create({header: 'Check your inbox', message: err, buttons: ['Okay']});
                await alertEl.present();

                return;
            }
        } else {
            imageFile = imageData;

            const loadingEl = await this.loadingCtrl.create({
                message: 'Uploading image to database...'
            });
            await loadingEl.present();
            this.uploadImageService.startUpload(imageFile).pipe(
                takeUntil(this.unsubscribe),
                catchError(async err => {
                    const alertEl = await this.alertCtrl.create({header: 'Check your inbox', message: err, buttons: ['Okay']});
                    await alertEl.present();
                })
            ).subscribe(url => {
                loadingEl.dismiss();
                if (url) {
                    form.patchValue({[attributeName]: url});
                    this.unsubscribe.next();
                }
            });

        }
    }

}
