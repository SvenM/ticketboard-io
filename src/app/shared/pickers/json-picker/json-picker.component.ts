import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
    selector: 'tb-json-picker',
    templateUrl: './json-picker.component.html',
    styleUrls: ['./json-picker.component.scss'],
})
export class JsonPickerComponent implements OnInit {

    @ViewChild('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
    @Output()
    public jsonPick: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit() {
    }

    onPickJSON() {
        this.filePickerRef.nativeElement.click();
    }

    onFileChosen(event: Event) {
        const pickedFile = (event.target as HTMLInputElement).files[0];
        if (!pickedFile) {
            return;
        }
        const fr = new FileReader();
        fr.onload = async () => {
            const json = await JSON.parse(fr.result.toString());
            this.jsonPick.emit(json);
        };
        fr.readAsText(pickedFile);
    }
}
