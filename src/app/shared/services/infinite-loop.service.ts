import { BehaviorSubject } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestoreCollectionGroup, Query } from '@angular/fire/firestore';
import { take, tap } from 'rxjs/operators';

export class InfiniteLoopService {
    constructor() {
        this._done = new BehaviorSubject<boolean>(false);
        this._loading = new BehaviorSubject<boolean>(false);
        this._data = new BehaviorSubject<any[]>([]);
    }

    // tslint:disable-next-line:variable-name
    protected _done: BehaviorSubject<boolean>;
    // tslint:disable-next-line:variable-name
    protected _loading: BehaviorSubject<boolean>;
    // tslint:disable-next-line:variable-name
    protected _data: BehaviorSubject<any[]>;

    protected query: Query;

    protected getCursor() {
        const current = this._data.value;
        if (current.length) {
            return current[current.length - 1].doc;
        }
        return null;
    }

    protected mapAndUpdate(col: AngularFirestoreCollectionGroup<any> | AngularFirestoreCollection, callback) {
        if (this._done.value || this._loading.value) {
            return;
        }

        // loading
        this._loading.next(true);

        return col.snapshotChanges()
            .pipe(
                tap(arr => {
                    const values = arr.map(callback);

                    this._data.next(values);
                    this._loading.next(false);

                    if (!values.length) {
                        this._done.next(true);
                    }
                }),
                take(1)
            ).subscribe();
    }
}
