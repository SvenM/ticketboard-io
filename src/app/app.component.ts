import { Component, OnInit } from '@angular/core';
import { Capacitor, Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    // private platform: Platform,
    // private statusBar: StatusBar,
  ) {
    // this.initializeApp();
  }

  public ngOnInit() {
  }

  // initializeApp() {
  //   this.platform.ready().then(() => {
  //     if (Capacitor.isPluginAvailable('SplashScreen')) {
  //       Plugins.SplashScreen.hide();
  //     }
  //     this.statusBar.styleDefault();
  //   });
  // }
}
