export interface PrivacySettings {
    email: boolean;
    birthday: boolean;
    location: boolean;
    phoneNumber: boolean;
    facebook: boolean;
    instagram: boolean;
    twitter: boolean;
    youtube: boolean;
}
