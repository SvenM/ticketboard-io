import { PrivacySettings } from './privacy-settings.model';

export interface Profile {
    id: string;
    address: string;
    bike: string;
    birthday: Date;
    city: string;
    country: string;
    email: string;
    favoriteTrack: string;
    firstName: string;
    isAdmin: boolean;
    lastName: string;
    nickname: string;
    profileImgUrl: string;
    phoneNumber: string;
    zip: string;
    privacySettings: PrivacySettings;
    unreadMessages?: number;

    facebook: string;
    instagram: string;
    twitter: string;
    youtube: string;
}
