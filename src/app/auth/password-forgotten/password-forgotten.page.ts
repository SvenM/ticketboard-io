import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Auth2Service } from '../auth2.service';
import { Router } from '@angular/router';

@Component({
  selector: 'tb-password-forgotten',
  templateUrl: './password-forgotten.page.html',
  styleUrls: ['./password-forgotten.page.scss'],
})
export class PasswordForgottenPage implements OnInit {

  form: FormGroup;

  constructor(private auth2Service: Auth2Service, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.email]
      })
  });
  }

  onSendRecoveryEmail() {
    this.auth2Service.recoverPassword(this.form.value.email).then(() => this.router.navigateByUrl('/auth'));
  }

}
