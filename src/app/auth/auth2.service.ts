import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { Observable, of } from 'rxjs';
import { take, catchError, switchMap, map, tap } from 'rxjs/operators';

import { Profile } from './profile.model';
import { User } from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class Auth2Service {
    public user: Observable<User>;
    private isLoading = false;

    constructor(
        private firebaseAuth: AngularFireAuth,
        private db: AngularFirestore,
        private router: Router,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
    ) {
        this.user = firebaseAuth.authState;
    }

    public async signup(email: string, password: string, username: string): Promise<void> {
        const loadingEl = await this.showLoadingSpinner();
        await loadingEl.present();

        const value = await firebase.auth()
            .createUserWithEmailAndPassword(email, password).catch(err => {
                this.isLoading = false;
                loadingEl.dismiss();
                this.showAlert(err.message);
                return null;
            });

        this.signUpSuccessful(value, username);
        this.isLoading = false;
        await loadingEl.dismiss();
        await this.checkNickname(value);

    }

    public async login(email: string, password: string): Promise<void> {

        const loadingEl = await this.showLoadingSpinner();
        await loadingEl.present();

        const value = await firebase.auth()
            .signInWithEmailAndPassword(email, password).catch(err => {
                this.isLoading = false;
                loadingEl.dismiss();
                this.showAlert(err.message);
                return null;
            });

        this.isLoading = false;
        await loadingEl.dismiss();
        await this.checkNickname(value);
    }

    public logout(): Promise<void> {
        return firebase.auth().signOut();
    }

    public async recoverPassword(passwordResetEmail) {
        await firebase.auth().sendPasswordResetEmail(passwordResetEmail).catch((error) => {
            this.showAlert(error.message);

            return null;
        });

        const message = 'A password reset email has been sent, please check your inbox.';
        const alertEl = await this.alertCtrl.create({header: 'Check your inbox', message, buttons: ['Okay']});
        await alertEl.present();
    }

    public async googleAuth(): Promise<any> {
        const loadingEl = await this.showLoadingSpinner();
        await loadingEl.present();

        const value = await this.authLogin(new firebase.auth.GoogleAuthProvider())
            .catch(err => {
                this.isLoading = false;
                loadingEl.dismiss();
                this.showAlert(err.message);
                return null;
            });

        this.signUpSuccessful(value);
        this.isLoading = false;
        await loadingEl.dismiss();
        await this.router.navigateByUrl('/core/events');

    }

    public async facebookAuth(): Promise<any> {
        const loadingEl = await this.showLoadingSpinner();
        await loadingEl.present();

        const value = await this.authLogin(new firebase.auth.FacebookAuthProvider())
            .catch(err => {
                this.isLoading = false;
                loadingEl.dismiss();
                this.showAlert(err.message);
                return null;
            });

        this.signUpSuccessful(value);
        this.isLoading = false;
        await loadingEl.dismiss();
        await this.router.navigateByUrl('/core/events');
    }

    private async authLogin(provider): Promise<any> {
        await firebase.auth().signInWithRedirect(provider)
            .catch(error => {
                this.showAlert(error.message);
            });
    }

    public isUserAdmin() {
        return this.user.pipe(switchMap((user: firebase.User) => {
            if (user && user.uid) {
                return this.db
                    .collection('users')
                    .doc(user.uid)
                    .get()
                    .pipe(
                        map(userRef => userRef.get('isAdmin')),
                        catchError(err => of(false))
                    );
            }

            return of(false);
        }));
    }

    private showLoadingSpinner() {
        this.isLoading = true;
        return this.loadingCtrl
            .create({keyboardClose: true, message: 'Logging in...'});
    }

    private async showAlert(message: string) {
        const alertEl = await this.alertCtrl.create({header: 'Authentication failed', message, buttons: ['Okay']});
        await alertEl.present();
    }

    private checkNickname(userData: firebase.auth.UserCredential): Promise<void> {
        return this.db
            .collection('users')
            .doc(userData.user.uid)
            .get()
            .pipe(
                take(1),
                tap(async userDoc => {
                    if (userDoc) {
                        if (!userDoc.data().nickname) {
                            await this.router.navigate(['/auth/create-nickname']);
                        } else {
                            await this.router.navigateByUrl('/core/events');
                        }
                    }
                }),
                catchError(() => of(null))
            )
            .toPromise();
    }

    public signUpSuccessful(userData: firebase.auth.UserCredential, username?: string) {
        this.db
            .collection('users')
            .doc(userData.user.uid)
            .get()
            .pipe(
                take(1),
                catchError(() => of(null))
            )
            .subscribe(async userDoc => {
                if (userDoc) {
                    if (!userDoc.exists) {
                        await this.db.collection('users').doc<Profile>(userData.user.uid).set({
                            id: userData.user.uid,
                            email: userData.user.email,
                            address: null,
                            bike: null,
                            birthday: null,
                            city: null,
                            country: null,
                            favoriteTrack: null,
                            firstName: null,
                            isAdmin: null,
                            lastName: null,
                            nickname: username || null,
                            phoneNumber: null,
                            profileImgUrl: null,
                            zip: null,
                            privacySettings: {
                                birthday: true,
                                email: true,
                                location: true,
                                phoneNumber: true,
                                facebook: true,
                                instagram: true,
                                twitter: true,
                                youtube: true
                            },
                            facebook: null,
                            instagram: null,
                            twitter: null,
                            youtube: null
                        });

                        if (!username) {
                            await this.router.navigate(['/auth/create-nickname']);
                        } else {
                            await this.router.navigate(['/core/events']);
                        }

                    } else {
                        if (!userDoc.data().nickname) {
                            await this.router.navigate(['/auth/create-nickname']);
                        } else {
                            await this.router.navigate(['/core/events']);
                        }
                    }
                }

            });
    }
}
