import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Auth2Service } from './auth2.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.page.html',
    styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
    public form: FormGroup;

    public isLoading = false;
    public isLogin = true;

    constructor(
        private auth2Service: Auth2Service,
        private router: Router,
        private alertCtrl: AlertController
    ) {
    }

    public async ngOnInit() {
        this.isLoading = true;
        const result = await firebase.auth().getRedirectResult().catch(err => {
            this.showAlert(err.message);
            this.isLoading = false;

            return null;
        });

        if (result?.user) {
            this.auth2Service.signUpSuccessful(result);
        } else {
            this.isLoading = false;
        }

        this.form = this.createLoginForm();

    }

    public async authenticate(email, password, username?) {
        if (this.isLogin) {
            await this.auth2Service.login(email, password);
        } else {
            await this.auth2Service.signup(email, password, username);
        }
    }

    public async onFacebookLogin() {
        await this.auth2Service.facebookAuth();
    }

    public async onGoogleLogin() {
        await this.auth2Service.googleAuth();
    }

    public async onPasswordForgotten() {
        await this.router.navigateByUrl('/auth/password-forgotten');
    }

    public onSubmit() {
        if (!this.form.valid) {
            this.form.markAllAsTouched();
            return;
        }
        const email = this.form.value.email;
        const password = this.form.value.password;
        const username = this.form.value.username || null;

        this.authenticate(email, password, username);
        this.form.reset();
    }

    public onSwitchAuthMode() {
        this.isLogin = !this.isLogin;
        if (this.isLogin) {
            this.form = this.createLoginForm();
        } else {
            this.form = this.createSignUpForm();
        }
    }

    private createSignUpForm() {
        return new FormGroup({
            email: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.email]
            }),
            password: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            }),
            username: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            })
        });
    }

    private createLoginForm() {
        return new FormGroup({
            email: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            password: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            })
        });
    }

    private async showAlert(message: string): Promise<void> {
        const alertEl = await this.alertCtrl.create({header: 'Authentication failed', message, buttons: ['Okay']});
        return alertEl.present();
    }
}
