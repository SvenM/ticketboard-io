import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsernameService } from './username.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { ProfileService } from '../../profile/profile.service';
import { Router } from '@angular/router';

@Component({
    selector: 'tb-create-nickname',
    templateUrl: './create-nickname.page.html',
    styleUrls: ['./create-nickname.page.scss'],
})
export class CreateNicknamePage implements OnInit {

    public form: FormGroup;

    constructor(
        private usernameService: UsernameService,
        private alertCtrl: AlertController,
        private profileService: ProfileService,
        private loadingCtrl: LoadingController,
        private router: Router
    ) {
    }

    public ngOnInit() {
        this.form = new FormGroup({
            nickname: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            })
        });
    }

    public async onSubmit() {
        if (!this.form.valid) {
            this.form.markAllAsTouched();
            return;
        }

        const loadingEl = await this.showLoadingSpinner();
        await loadingEl.present();
        const username = await this.usernameService.checkUsernameDuplicate(this.form.value.nickname);

        const messageNoInput = 'Your did not provide a valid username.';
        const message = 'Unfortunately this nickname already exists. Please pick a different name.';

        if (!username) {
            this.destroyLoadingSpinner(loadingEl);
            const alertEl = await this.alertCtrl
                .create({header: 'Error', message: messageNoInput, buttons: ['Okay']});
            await alertEl.present();
        } else if (username.exists) {
            this.destroyLoadingSpinner(loadingEl);
            const alertEl = await this.alertCtrl.create({
                header: 'Username already in use.',
                message,
                buttons: ['Okay']
            });
            await alertEl.present();
        } else {
            await this.usernameService.createNewUsername(this.form.value.nickname);
            this.destroyLoadingSpinner(loadingEl);
            await this.router.navigateByUrl('/core/events');
        }
    }

    private showLoadingSpinner() {
        return this.loadingCtrl
            .create({keyboardClose: true, message: 'Validating username...'});
    }

    private destroyLoadingSpinner(loadingEl) {
        if (loadingEl) {
            loadingEl.dismiss();
            loadingEl = null;
        }
    }
}
