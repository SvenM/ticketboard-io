import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateNicknamePage } from './create-nickname.page';

const routes: Routes = [
  {
    path: '',
    component: CreateNicknamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateNicknamePageRoutingModule {}
