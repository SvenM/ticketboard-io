import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateNicknamePageRoutingModule } from './create-nickname-routing.module';

import { CreateNicknamePage } from './create-nickname.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CreateNicknamePageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [CreateNicknamePage]
})
export class CreateNicknamePageModule {
}
