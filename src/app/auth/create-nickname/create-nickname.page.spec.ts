import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateNicknamePage } from './create-nickname.page';

describe('CreateNicknamePage', () => {
  let component: CreateNicknamePage;
  let fixture: ComponentFixture<CreateNicknamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNicknamePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateNicknamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
