import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Auth2Service } from '../auth2.service';
import { ProfileService } from '../../profile/profile.service';
import * as firebase from 'firebase';
import { take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UsernameService {

    private USERNAME_COLLECTION = 'usernames';

    constructor(
        private db: AngularFirestore,
        private authService: Auth2Service,
        private profileService: ProfileService
    ) {
    }

    public checkUsernameDuplicate(nickname: string): Promise<any> {
        if (nickname) {
            return this.db.collection(this.USERNAME_COLLECTION).doc(nickname).get().toPromise();
        }

        return new Promise<any>(null);
    }

    public async createNewUsername(nickname: string): Promise<void> {
        console.log(nickname);
        await this.db.collection(this.USERNAME_COLLECTION).doc(nickname).set({exists: true});
        this.authService.user.pipe(take(1)).subscribe(user => {
            console.log(user);
            return this.profileService.changeNickname(user.uid, nickname);
        });
    }
}
