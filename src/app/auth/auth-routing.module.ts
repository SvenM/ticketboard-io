import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthPage } from './auth.page';

const routes: Routes = [
  {
    path: '',
    component: AuthPage
  },
  {
    path: 'password-forgotten',
    loadChildren: () => import('./password-forgotten/password-forgotten.module').then( m => m.PasswordForgottenPageModule)
  },
  {
    path: 'create-nickname',
    loadChildren: () => import('./create-nickname/create-nickname.module').then( m => m.CreateNicknamePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthPageRoutingModule { }
