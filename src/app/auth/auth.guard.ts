import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import { UrlSegment, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, tap, map } from 'rxjs/operators';

import { Auth2Service } from './auth2.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad {
    constructor(private auth2Service: Auth2Service, private router: Router) {

    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        return this.auth2Service.user.pipe(
            take(1),
            map(user => !!user),
            tap(isAuthenticated => {
                if (!isAuthenticated) {
                    this.router.navigateByUrl('/auth');
                }
            }));
    }
}
