import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Auth2Service } from '../auth/auth2.service';
import { User } from 'firebase';
import { MessagesService } from './messenger/messages.service';


@Component({
    selector: 'app-core',
    templateUrl: './core.page.html',
    styleUrls: ['./core.page.scss'],
})
export class CorePage implements OnInit {

    public userIsAdmin$: Observable<boolean>;
    public userIsAuthenticated$: Observable<User>;
    public unreadMessages$: Observable<number>;

    constructor(private auth2Service: Auth2Service, private messagesService: MessagesService) {
    }

    ngOnInit() {
        this.userIsAuthenticated$ = this.auth2Service.user;
        this.userIsAdmin$ = this.auth2Service.isUserAdmin();
        this.unreadMessages$ = this.messagesService.getTotalUnreadMessages();

    }

}
