import { Injectable } from '@angular/core';
import { Promoter } from './promoter.model';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { take, map, scan } from 'rxjs/operators';
import { SharedService } from '../../shared/shared.service';
import { Event } from '../events/event.model';
import { InfiniteLoopService } from '../../shared/services/infinite-loop.service';

@Injectable({
    providedIn: 'root'
})
export class PromoterService extends InfiniteLoopService {

    private PROMOTERS_COLLECTION = 'promoters';
    private EVENTS_COLLECTION = 'events';

    public data: Observable<Partial<Promoter>[]>;
    public done: Observable<boolean> = this._done.asObservable();
    public loading: Observable<boolean> = this._loading.asObservable();

    constructor(
        private db: AngularFirestore,
        private sharedService: SharedService,

    ) {
        super();
        this.query = this.db
            .collection<Partial<Promoter>[]>(this.PROMOTERS_COLLECTION)
            .ref
            .limit(15)
            .orderBy('name');

    }

    public init() {
        const first = this.db.collection(this.PROMOTERS_COLLECTION, ref => this.query);

        this._data.next([]);
        this._done.next(false);
        this.mapAndUpdate(first, this.updateCallback);

        this.data = this._data.asObservable().pipe(scan((acc, val) => {
            return acc.concat(val);
        }));
    }

    public more() {
        const cursor = this.getCursor();

        const more = this.db.collection(this.PROMOTERS_COLLECTION, ref => this.query.startAfter(cursor));
        this.mapAndUpdate(more, this.updateCallback);
    }

    private updateCallback = snap => {
        const data = {
            id: snap.payload.doc.id,
            ...snap.payload.doc.data(),
        };
        const doc = snap.payload.doc;
        return {...data, doc};
    };

    private changeFilter() {
        this.query = this.db
            .collection<Partial<Promoter>[]>(this.PROMOTERS_COLLECTION)
            .ref
            .limit(15)
            .orderBy('name');
        this._data.next([]);
        this.init();
    }

    public addPromoter(promoter: Partial<Promoter>): Promise<DocumentReference> {
        return this.db.collection(this.PROMOTERS_COLLECTION).add(promoter);
    }

    public createEmptyPromoter(): Promise<DocumentReference> {
        const promoter: Partial<Promoter> = {
            address: null,
            city: null,
            contact: null,
            description: null,
            email: null,
            homepage: null,
            logoUrl: null,
            name: null,
            rating: null,
            telephone: null,
            zip: null
        };

        return this.db.collection(this.PROMOTERS_COLLECTION).add(promoter);
    }

    public getPromoter(promoterId: string): Promise<Partial<Promoter>> {
        return this.db.collection(this.PROMOTERS_COLLECTION).doc(promoterId).get().pipe(
            take(1),
            map(promoter => {
                return {
                    id: promoter.id,
                    ...promoter.data()
                };
            })
        ).toPromise();
    }

    public getAllPromoters(): Observable<Partial<Promoter>[]> {
        return this.db.collection<Partial<Promoter>>(this.PROMOTERS_COLLECTION, ref =>
            ref.orderBy('name')
        ).valueChanges({idField: 'id'}).pipe(
            map(data => {
                return data.map(doc => ({
                    ...doc
                }));
            })
        );
    }

    public getPromoterEvents(promoterId: string): Observable<Partial<Event>[]> {
        return this.db
            .collection(this.PROMOTERS_COLLECTION)
            .doc(promoterId)
            .collection(this.EVENTS_COLLECTION, ref => ref.orderBy('dateFrom').limit(5)
                .where('dateFrom', '>=', this.sharedService.createDateFrom(new Date(Date.now()))))
            .valueChanges({idField: 'id'})
            .pipe(
                map(data => {
                    return data.map(e => {
                        return {
                            ...e,
                            dateFrom: e.dateFrom ? (e.dateFrom as any).toDate() : null,
                            dateTo: e.dateTo ? (e.dateTo as any).toDate() : null
                        };
                    });
                })
            );
    }

    public deletePromoter(promoterId: string): Promise<void> {
        const promoterRef = this.db.collection(this.PROMOTERS_COLLECTION).doc(promoterId).ref;
        return promoterRef.delete();
    }

    public updatePromoter(promoterId: string, data: Partial<Promoter>): Promise<void> {
        const promoterRef = this.db.collection(this.PROMOTERS_COLLECTION).doc(promoterId).ref;
        return promoterRef.update(data);
    }

    public uploadFromJson(jsonPromoters: Partial<Promoter>[]) {
        const batch = this.db.firestore.batch();

        jsonPromoters.forEach(promoter => {
            const promoterId = this.sharedService.generateFirestoreId();
            const docRef = this.db.collection(this.PROMOTERS_COLLECTION).doc(promoterId).ref;
            batch.set(docRef, {
                id: promoterId || null,
                name: promoter.name || null,
                logoUrl: null,
                homepage: promoter.homepage || null,
                contact: promoter.contact || null,
                address: promoter.address || null,
                zip: promoter.zip || null,
                city: promoter.city || null,
                telephone: promoter.telephone || null,
                email: promoter.email || null,
                description: promoter.description || null,
            });
        });

        return batch.commit();
    }
}
