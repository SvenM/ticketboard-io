import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotersPage } from './promoters.page';

describe('PromotersPage', () => {
  let component: PromotersPage;
  let fixture: ComponentFixture<PromotersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PromotersPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
