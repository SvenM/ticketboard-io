import { Component, OnInit } from '@angular/core';

import { Promoter } from './promoter.model';
import { PromoterService } from './promoter.service';

@Component({
    selector: 'app-promoters',
    templateUrl: './promoters.page.html',
    styleUrls: ['./promoters.page.scss'],
})
export class PromotersPage implements OnInit {


    constructor(public promoters$: PromoterService) {
    }

    public ngOnInit() {
        this.promoters$.init();

    }

    public ionViewWillEnter() {
        this.promoters$.init();
    }

    public trackByPromoterId(_, promoter: Promoter) {
        return promoter.id;
    }
}
