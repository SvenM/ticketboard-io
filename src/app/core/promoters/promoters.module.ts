import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PromotersPageRoutingModule } from './promoters-routing.module';
import { PromotersPage } from './promoters.page';
import { PromoterItemComponent } from './promoter-item/promoter-item.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PromotersPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [PromotersPage, PromoterItemComponent],
    exports: [PromoterItemComponent]
})
export class PromotersPageModule {
}
