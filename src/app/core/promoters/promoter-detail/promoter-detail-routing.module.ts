import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromoterDetailPage } from './promoter-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PromoterDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromoterDetailPageRoutingModule { }
