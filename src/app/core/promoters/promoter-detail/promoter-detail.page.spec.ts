import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromoterDetailPage } from './promoter-detail.page';

describe('promoterDetailPage', () => {
  let component: PromoterDetailPage;
  let fixture: ComponentFixture<PromoterDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PromoterDetailPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromoterDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
