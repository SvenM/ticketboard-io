import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromoterDetailPageRoutingModule } from './promoter-detail-routing.module';

import { PromoterDetailPage } from './promoter-detail.page';
import { EventsPageModule } from '../../events/events.module';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PromoterDetailPageRoutingModule,
        EventsPageModule,
        SharedModule
    ],
  declarations: [PromoterDetailPage]
})
export class PromoterDetailPageModule { }
