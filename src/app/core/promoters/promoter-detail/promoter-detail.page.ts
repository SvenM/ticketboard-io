import { Component, OnInit } from '@angular/core';
import { Promoter } from '../promoter.model';
import { ActivatedRoute } from '@angular/router';
import { PromoterService } from '../promoter.service';
import { take, tap } from 'rxjs/operators';
import { AlertController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Event } from '../../events/event.model';
import { DetailPageComponent } from '../../../shared/components/classes/detail-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { tracksGetTrackAction, tracksGetTrackEventsAction } from '../../../state-management/tracks/tracks.actions';
import { selectTracksStateEvents, selectTracksStateSelectedTrack } from '../../../state-management/tracks/tracks.selectors';
import { promotersGetPromoterAction, promotersGetPromoterEventsAction } from '../../../state-management/promoters/promoters.actions';
import { selectPromotersStateEvents, selectPromotersStateSelectedPromoter } from '../../../state-management/promoters/promoters.selectors';

@Component({
    selector: 'app-promoter-detail',
    templateUrl: './promoter-detail.page.html',
    styleUrls: ['./promoter-detail.page.scss'],
})
export class PromoterDetailPage extends DetailPageComponent implements OnInit {

    public isLoading = false;
    public promoter$: Observable<Promoter>;
    public events$: Observable<Partial<Event>[]>;

    constructor(
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        private route: ActivatedRoute,
        private promoterService: PromoterService,
        private store: Store<AppState>
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {

        const id = this.route.snapshot.params.id;

        if (!id) {
            await this.navigateBack();
            return;
        }
        this.isLoading = true;

        this.isLoading = true;
        this.events$ = this.store.select(selectPromotersStateEvents);
        this.promoter$ = this.store.select(selectPromotersStateSelectedPromoter).pipe(
            tap(promoter => {
                    if (promoter) {
                        this.isLoading = false;
                    }
                }
            )
        );
    }

    public trackByEventId(_, event: Event) {
        return event.id;
    }
}
