import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromoterItemComponent } from './promoter-item.component';

describe('promoterItemComponent', () => {
  let component: PromoterItemComponent;
  let fixture: ComponentFixture<PromoterItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PromoterItemComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromoterItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
