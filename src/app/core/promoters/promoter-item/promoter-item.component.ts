import { Component, Input } from '@angular/core';

import { Promoter } from '../promoter.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { ActivatedRoute, Router } from '@angular/router';
import { promotersGetPromoterAction, promotersGetPromoterEventsAction } from '../../../state-management/promoters/promoters.actions';

@Component({
  selector: 'app-promoter-item',
  templateUrl: './promoter-item.component.html',
  styleUrls: ['./promoter-item.component.scss'],
})
export class PromoterItemComponent {

  @Input() data: Promoter;

  constructor(private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute) { }

  public async onShowDetails() {
    await this.store.dispatch(promotersGetPromoterAction({id: this.data.id}));
    await this.store.dispatch(promotersGetPromoterEventsAction({id: this.data.id}));
    await this.router.navigate([this.data.id], {relativeTo: this.activatedRoute});
  }


}
