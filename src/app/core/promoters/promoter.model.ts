export interface Promoter {
    id: string;
    name: string;
    logoUrl: string;
    homepage: string;
    contact: string;
    address: string;
    zip: string;
    city: string;
    telephone: string;
    email: string;
    rating: number;
    description: string;
}
