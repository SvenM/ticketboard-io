import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';
import { Action, AngularFirestore, DocumentSnapshot } from '@angular/fire/firestore';
import { Auth2Service } from '../../auth/auth2.service';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from '@angular/fire/database';
import { r_Chat } from './chat.model';

@Injectable({
    providedIn: 'root'
})
export class ChatService {

    constructor(
        private db: AngularFirestore,
        private authService: Auth2Service,
        private alertCtrl: AlertController,
        private realtimeDb: AngularFireDatabase
    ) {
    }

    public getChatrooms(): Observable<r_Chat[]> {
        return this.authService.user.pipe(
            take(1),
            switchMap((user: firebase.User) => {
                return this.realtimeDb.list<r_Chat>(`/chats/${ user.uid }`, ref => ref.orderByChild('lastMessageTimestamp')).valueChanges();
            })
        );
    }

    public getChatPartnerDetails(chatId: string): Observable<any> {
        return this.authService.user.pipe(
            take(1),
            switchMap((user: firebase.User) => {
                const ref = this.realtimeDb.database.ref(`/chats/${ user.uid }/${ chatId }/chatPartner`);
                return this.realtimeDb.object(ref).valueChanges().pipe(take(1));
            })
        );
    }

    public createChatroom(listing: Partial<any> | Partial<any>): Promise<any> {
        return this.authService.user.pipe(
            take(1),
            map((user: firebase.User) => {
                return user.uid;
            }),
            switchMap(userId => {
                if (userId === listing.userId) {
                    throw new Error();
                }

                const userDocs = [userId, listing.userId].map(u => this.db.doc('users/' + u).snapshotChanges());

                return userDocs.length ? combineLatest(userDocs) : of([]);
            }),
            take(1),
            switchMap((arr: Action<DocumentSnapshot<any>>[]) => {
                if (arr.length === 2) {
                    const id1 = arr[0].payload.id;
                    const doc1 = arr[0].payload.data();

                    const id2 = arr[1].payload.id;
                    const doc2 = arr[1].payload.data();

                    const newChatId = this.realtimeDb.createPushId();

                    const chatUser1 = {
                        id: newChatId,
                        lastMessage: null,
                        lastMessageTimeStamp: null,
                        unreadMessages: 0,
                        chatPartner: {
                            id: id2,
                            username: doc2.nickname,
                            profileImg: doc2.profileImgUrl
                        },
                        listingEvent: {
                            ...listing.event
                        }
                    };

                    const chatUser2 = {
                        id: newChatId,
                        lastMessage: null,
                        lastMessageTimeStamp: null,
                        unreadMessages: 0,
                        chatPartner: {
                            id: id1,
                            username: doc1.nickname,
                            profileImg: doc1.profileImgUrl
                        },
                        listingEvent: {
                            ...listing.event
                        }
                    };

                    const updates = {};
                    updates['/chats/' + id1 + '/' + chatUser1.id] = chatUser1;
                    updates['/chats/' + id2 + '/' + chatUser2.id] = chatUser2;

                    return this.realtimeDb.database.ref().update(updates).then(() => true);
                }
            }),
            catchError(async () => {
                const el = await this.alertCtrl.create({
                    header: 'Error',
                    message: 'You cannot contact yourself',
                    buttons: ['Okay']
                });
                await el.present();
                return of(null);
            })
        ).toPromise();
    }

    public resetUnreadMessages(chatId: string) {
        return this.authService.user.pipe(
            take(1),
            tap(async (user: firebase.User) => {
                const chatUnreadMessageRef = this.realtimeDb.database.ref(`/chats/${ user.uid }/${ chatId }/unreadMessages`);
                const userTotalUnreadMessageRef = this.realtimeDb.database.ref(`/users/${ user.uid }/unreadMessages`);

                let deduction = 0;
                await chatUnreadMessageRef.transaction(unreadMessages => {
                    if (unreadMessages) {
                        deduction = unreadMessages;
                        unreadMessages = 0;
                    }
                    return unreadMessages;
                });
                await userTotalUnreadMessageRef.transaction(unreadMessages => {
                    if (unreadMessages) {
                        unreadMessages = unreadMessages - deduction;
                    }
                    return unreadMessages;
                });
            })
        );
    }
}
