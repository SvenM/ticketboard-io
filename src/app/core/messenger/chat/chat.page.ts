import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ChatService } from '../chat.service';
import { ChatPartner } from '../chat.model';
import { Observable } from 'rxjs';
import { MessagesService } from '../messages.service';
import { Auth2Service } from '../../../auth/auth2.service';
import { take, tap, withLatestFrom } from 'rxjs/operators';
import { Message, MessageItem, r_MessageItem } from '../message.model';

@Component({
    selector: 'tb-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

    @ViewChild('content') content;

    public form: FormGroup;
    public chatPartner: ChatPartner;

    public messages$: Observable<MessageItem[]>;

    constructor(
        private route: ActivatedRoute,
        private navCtrl: NavController,
        private chatService: ChatService,
        private messagesService: MessagesService,
        private authService: Auth2Service,
    ) {
    }

    public ngOnInit() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            this.navigateBack();
            return;
        }

        this.chatService.getChatPartnerDetails(id).pipe(take(1)).subscribe(chatPartner => {
            this.chatPartner = chatPartner;
        });
        this.messages$ = this.messagesService.listenToMessages(id).pipe(
            tap((m) => {
                this.content.scrollToBottom(500).then(() => this.content.scrollToBottom(500));
            })
        );
        this.chatService.resetUnreadMessages(id).subscribe();


        this.form = new FormGroup({
            message: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            })
        });
    }

    public onSubmit() {
        if (!this.form.valid) {
            return;
        } else {
            this.authService.user.pipe(
                tap(user => {
                    const message: Message = {
                        createdAt: Date.now(),
                        userId: user.uid,
                        message: this.form.value.message
                    };

                    this.messagesService.sendMessage(this.route.snapshot.params.id, message, this.chatPartner.id);
                }),
                take(1)
            ).subscribe(() => this.form.reset());
        }
    }

    private navigateBack() {
        this.navCtrl.back();
    }
}
