import { Component, Input, OnInit } from '@angular/core';
import { Message, MessageItem, r_MessageItem } from '../../message.model';

@Component({
  selector: 'tb-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {

  @Input() data: r_MessageItem;
  @Input() username: string;

  constructor() { }

  ngOnInit() {}

}
