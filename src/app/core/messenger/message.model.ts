export interface Message {
    userId: string;
    message: string;
    createdAt: number;
}

export interface MessageItem {
    ownMessage: boolean;
    message: string;
    createdAt: number;
}

export interface r_MessageItem {
    content: string;
    timestamp: number;
    sender: r_Sender;
    ownMessage: boolean;
}

export interface r_Message {
    content: string;
    timestamp: number;
    sender: r_Sender
}

export interface r_Sender {
    uid: string;
    name: string;
}
