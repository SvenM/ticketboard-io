import { Event } from '../events/event.model';

export interface Chat {
    id: string;
    users: string[];
    lastMessage: string;
    lastMessageDate: Date;
}

export interface ChatItem {
    id: string,
    lastMessage: string;
    lastMessageDate: Date;
    chatPartner: ChatPartner,
    unreadMessages: number
}

export interface ChatPartner {
    id: string;
    profileImg: string;
    username: string
}

export interface r_Chat {
    id: string;
    lastMessage: string;
    lastMessageTimeStamp: Date;
    unreadMessages: number;
    chatPartner: ChatPartner;
    listingEvent: Event;
}
