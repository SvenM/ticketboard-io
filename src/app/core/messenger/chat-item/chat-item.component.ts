import { Component, Input } from '@angular/core';
import { ChatItem, r_Chat } from '../chat.model';

@Component({
  selector: 'tb-chat-item',
  templateUrl: './chat-item.component.html',
  styleUrls: ['./chat-item.component.scss'],
})
export class ChatItemComponent {

  @Input() data: r_Chat;

}
