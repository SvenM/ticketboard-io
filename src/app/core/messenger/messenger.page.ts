import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';
import { Observable } from 'rxjs';
import { r_Chat } from './chat.model';

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.page.html',
  styleUrls: ['./messenger.page.scss'],
})
export class MessengerPage implements OnInit {

  public r_chats$: Observable<r_Chat[]>;

  constructor(public chats$: ChatService) { }

  public ngOnInit() {
    this.r_chats$ = this.chats$.getChatrooms();
  }

  public ionViewWillEnter() {
  }
}
