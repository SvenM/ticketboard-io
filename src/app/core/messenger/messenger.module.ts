import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { MessengerPageRoutingModule } from './messenger-routing.module';
import { MessengerPage } from './messenger.page';
import { ChatItemComponent } from './chat-item/chat-item.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MessengerPageRoutingModule,
        SharedModule
    ],
  declarations: [MessengerPage, ChatItemComponent]
})
export class MessengerPageModule { }
