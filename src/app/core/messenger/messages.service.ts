import { Injectable } from '@angular/core';
import { Message, r_Message } from './message.model';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { Auth2Service } from '../../auth/auth2.service';
import * as firebase from 'firebase/app';
import { ProfileService } from '../../profile/profile.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Profile } from '../../auth/profile.model';


@Injectable({
    providedIn: 'root'
})
export class MessagesService {

    constructor(
        private authService: Auth2Service,
        private profileService: ProfileService,
        private realtimeDb: AngularFireDatabase
    ) {
    }

    public getTotalUnreadMessages(): Observable<number> {
        return this.authService.user.pipe(
            switchMap((user: firebase.User) => {
                if (!user) {
                    return of(null);
                }
                const userRef = this.realtimeDb.database.ref('/users/' + user.uid + '/unreadMessages');
                return this.realtimeDb.object<number>(userRef).valueChanges()
            })
        );
    }

    public listenToMessages(chatId: string): Observable<any[]> {
        return this.authService.user.pipe(
            take(1),
            switchMap((user: firebase.User) => {
                return this.realtimeDb
                    .list(`/messages/${ chatId }`, ref => ref.orderByChild('timestamp').limitToLast(30))
                    .valueChanges().pipe(
                        map(messages => messages.map((message: r_Message) => {
                                return {
                                    ...message,
                                    ownMessage: message.sender.uid === user.uid
                                }
                            })
                        )
                    );
        })
        )

    }

    public sendMessage(chatId: string, message: Message, chatPartnerId) {
        return this.authService.user.pipe(
            take(1),
            switchMap((user: firebase.User) => {
                return this.profileService.getProfile(user.uid);
            }),
            take(1),
            switchMap((profile: Profile) => {

                const newMessageId = this.realtimeDb.createPushId();

                const messageItem = {
                    content: message.message,
                    timestamp: message.createdAt,
                    sender: {
                        uid: profile.id,
                        name: profile.nickname
                    }
                };

                const chatRef = this.realtimeDb.database.ref(`/chats/${ chatPartnerId }/${ chatId }`);
                const userTotalUnreadMessageRef = this.realtimeDb.database.ref(`/users/${ chatPartnerId }/unreadMessages`);

                const updates = {};
                updates[`/messages/${ chatId }/'${ newMessageId }`] = messageItem;

                this.realtimeDb.database.ref().update(updates);
                chatRef.transaction(chat => {
                    if (chat) {
                        if (chat.unreadMessages) {
                            chat.unreadMessages++;
                        } else {
                            chat.unreadMessages = 1;
                        }
                        chat.lastMessage = messageItem.content;
                        chat.lastMessageTimeStamp = messageItem.timestamp;
                    }
                    return chat;
                });
                userTotalUnreadMessageRef.transaction(unreadMessages => {
                    if (unreadMessages) {
                        unreadMessages++;
                    } else {
                        unreadMessages = 1;
                    }
                    return unreadMessages;
                });

                return of(true);
            })
        ).toPromise();
    }
}
