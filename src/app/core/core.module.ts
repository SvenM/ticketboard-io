import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CorePageRoutingModule } from './core-routing.module';
import { CorePage } from './core.page';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorePageRoutingModule,
    SharedModule,
  ],
  declarations: [CorePage]
})
export class CorePageModule { }
