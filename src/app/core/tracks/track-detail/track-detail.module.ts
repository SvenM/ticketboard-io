import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrackDetailPageRoutingModule } from './track-detail-routing.module';

import { TrackDetailPage } from './track-detail.page';
import { SharedModule } from '../../../shared/shared.module';
import { EventsPageModule } from '../../events/events.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TrackDetailPageRoutingModule,
        SharedModule,
        EventsPageModule
    ],
  declarations: [TrackDetailPage]
})
export class TrackDetailPageModule {}
