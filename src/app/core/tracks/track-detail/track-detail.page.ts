import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { TrackService } from '../track.service';
import { map, tap } from 'rxjs/operators';
import { Track } from '../track.model';
import { Event } from '../../events/event.model';
import { Observable } from 'rxjs';
import { DetailPageComponent } from '../../../shared/components/classes/detail-page.component';
import { AppState } from '../../../app.module';
import { Store } from '@ngrx/store';
import {
    selectTracksState,
    selectTracksStateEvents,
    selectTracksStateSelectedTrack
} from '../../../state-management/tracks/tracks.selectors';
import { tracksGetTrackAction, tracksGetTrackEventsAction } from '../../../state-management/tracks/tracks.actions';

@Component({
    selector: 'app-track-detail',
    templateUrl: './track-detail.page.html',
    styleUrls: ['./track-detail.page.scss'],
})
export class TrackDetailPage extends DetailPageComponent implements OnInit {

    public isLoading = false;
    public track$: Observable<Track>;
    public events$: Observable<Partial<Event>[]>;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private trackService: TrackService,
        private store: Store<AppState>
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            await this.navigateBack();
            return;
        }
        this.isLoading = true;
        this.events$ = this.store.select(selectTracksStateEvents);
        this.track$ = this.store.select(selectTracksStateSelectedTrack).pipe(
            tap(track => {
                    if (track) {
                        this.isLoading = false;
                    }
                }
            )
        );
    }

    public trackByEventId(_, event: Event) {
        return event.id;
    }
}
