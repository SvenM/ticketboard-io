export interface Track {
    id: string;
    name: string;
    address: string;
    zip: string;
    city: string;
    country: string;
    homepage: string;
    length: string;
    width: string;
    drivingDirection: string;
    numberOfTurns: number;
    layoutImgUrl: string;
    dBLimit: number;
    description: string;
}
