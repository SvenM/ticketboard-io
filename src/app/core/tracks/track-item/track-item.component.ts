import { Component, Input } from '@angular/core';

import { Track } from '../track.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { ActivatedRoute, Router } from '@angular/router';
import { tracksGetTrackAction, tracksGetTrackEventsAction } from '../../../state-management/tracks/tracks.actions';

@Component({
  selector: 'app-track-item',
  templateUrl: './track-item.component.html',
  styleUrls: ['./track-item.component.scss'],
})
export class TrackItemComponent {

  @Input() data: Track;

  constructor(private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute) { }

  public async onShowDetails() {
    await this.store.dispatch(tracksGetTrackAction({id: this.data.id}));
    await this.store.dispatch(tracksGetTrackEventsAction({id: this.data.id}));
    await this.router.navigate([this.data.id], {relativeTo: this.activatedRoute});
  }
}
