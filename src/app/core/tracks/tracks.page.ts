import { Component, OnInit } from '@angular/core';

import { Track } from './track.model';
import { TrackService } from './track.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { tracksFilterResetAllFiltersAction, tracksFilterSetAllFiltersAction } from '../../state-management/tracksFilter/tracks-filter.actions';
import { PickerController } from '@ionic/angular';
import { CountryFilterSelect } from '../../shared/models/filter-select.model';

@Component({
    selector: 'app-tracks',
    templateUrl: './tracks.page.html',
    styleUrls: ['./tracks.page.scss'],
})
export class TracksPage implements OnInit {

    public countryFilter: any;

    constructor(
        public tracks$: TrackService,
        private store: Store<AppState>,
        private pickerCtrl: PickerController
    ) {
    }

    public ngOnInit() {
        this.tracks$.init();
    }

    public ionViewWillEnter() {
        this.tracks$.init();
    }

    public trackByTrackId(_, track: Track) {
        return track.id;
    }

    public onClearFilter() {
        this.countryFilter = null;
        this.store.dispatch(tracksFilterResetAllFiltersAction());
        this.tracks$.changeFilter();
    }

    private createCountryPickerColumns() {
        return CountryFilterSelect.map(c => ({ text: c.selectorText, value: c.key}));
    }

    public async onOpenCountrySelector() {
        const el = await this.pickerCtrl.create({
            columns: [{name: 'country', options: this.createCountryPickerColumns()}],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Confirm',
                    handler: (value) => {
                        this.countryFilter = value.country.value;
                        this.onCountrySelected();
                    }
                }
            ]
        });
        await el.present();
    }



    public onCountrySelected() {
        this.store.dispatch(tracksFilterSetAllFiltersAction({
            filters: {
                country: {
                    name: 'country',
                    operation: '==',
                    value: this.countryFilter ? this.countryFilter : null
                }
            }
        }));
        this.tracks$.changeFilter();
    }
}
