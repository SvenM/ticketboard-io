import { Injectable } from '@angular/core';
import {
    AngularFirestore, DocumentChangeAction,
    DocumentReference,
} from '@angular/fire/firestore';
import { Track } from './track.model';
import { Observable } from 'rxjs';
import { take, map, tap, scan } from 'rxjs/operators';
import { SharedService } from '../../shared/shared.service';
import { Event } from '../events/event.model';
import { Store } from '@ngrx/store';

import { selectTracksFiltersFilters } from '../../state-management/tracksFilter/tracks-filter.selectors';
import { AppState } from '../../app.module';

import * as firebase from 'firebase/app';
import { InfiniteLoopService } from '../../shared/services/infinite-loop.service';

@Injectable({
    providedIn: 'root'
})
export class TrackService extends InfiniteLoopService {

    private TRACKS_COLLECTION = 'tracks';
    private EVENTS_COLLECTION = 'events';

    private fs = firebase.firestore();

    public data: Observable<Partial<Track>[]>;
    public done: Observable<boolean> = this._done.asObservable();
    public loading: Observable<boolean> = this._loading.asObservable();

    constructor(
        private db: AngularFirestore,
        private sharedService: SharedService,
        private store: Store<AppState>
    ) {
        super();

        this.query = this.fs
            .collectionGroup(this.TRACKS_COLLECTION)
            .limit(15)
            .orderBy('name');

    }

    public init() {
        const first = this.db.collectionGroup(this.TRACKS_COLLECTION, ref => this.query);

        this._data.next([]);
        this._done.next(false);
        this.mapAndUpdate(first, this.updateCallback);

        this.data = this._data.asObservable().pipe(scan((acc, val) => {
            return acc.concat(val);
        }));
    }

    public more() {
        const cursor = this.getCursor();

        const more = this.db.collectionGroup(this.TRACKS_COLLECTION, ref => this.query.startAfter(cursor));
        this.mapAndUpdate(more, this.updateCallback);
    }

    private updateCallback = snap => {
        const data = {
            id: snap.payload.doc.id,
            ...snap.payload.doc.data(),
        };
        const doc = snap.payload.doc;
        return {...data, doc};
    }

    public changeFilter() {
        this.store.select(selectTracksFiltersFilters).pipe(
            tap(filters => {
                let query = this.fs
                    .collectionGroup(this.TRACKS_COLLECTION)
                    .limit(15)
                    .orderBy('name');

                if (filters) {
                    filters.forEach(filter => {
                        if (filter.value) {
                            query = query.where(filter.name, filter.operation, filter.value);
                        }
                    });
                }

                this.query = query;
                this.init();
            }),
            take(1)
        ).subscribe();
    }

    public addTrack(track: Partial<Track>): Promise<DocumentReference> {
        return this.db.collection(this.TRACKS_COLLECTION).add(track);
    }

    public deleteTrack(trackId: string): Promise<void> {
        const trackRef = this.db.collection(this.TRACKS_COLLECTION).doc(trackId).ref;
        return trackRef.delete();
    }

    public updateTrack(trackId: string, data: Partial<Track>): Promise<void> {
        const trackRef = this.db.collection(this.TRACKS_COLLECTION).doc(trackId).ref;
        return trackRef.update(data);
    }

    public getTrack(trackId: string): Promise<Partial<Track>> {
        return this.db.collection(this.TRACKS_COLLECTION).doc(trackId).get().pipe(
            map(track => {
                return {
                    id: trackId,
                    ...track.data()
                };
            })
        ).toPromise();
    }

    public getAllTracks(): Observable<Partial<Track>[]> {
        return this.db.collection(this.TRACKS_COLLECTION, ref => ref.orderBy('name')).valueChanges({idField: 'id'}).pipe(
            map(data => {
                return data.map(d => ({
                    ...d
                }));
            })
        );
    }

    public getTrackEvents(trackId: string): Observable<Partial<Event>[]> {
        return this.db
            .collectionGroup(this.EVENTS_COLLECTION, ref => ref
                .orderBy('dateFrom')
                .limit(5)
                .where('trackId', '==', trackId)
                .where('dateFrom', '>=', this.sharedService.createDateFrom(new Date(Date.now()))))
            .snapshotChanges()
            .pipe(
                map(snaps => {
                    return snaps.map((snap: DocumentChangeAction<Event>) => ({
                            id: snap.payload.doc.id,
                            ...snap.payload.doc.data(),
                            dateFrom: snap.payload.doc.data().dateFrom ? (snap.payload.doc.data().dateFrom as any).toDate() : null,
                            dateTo: snap.payload.doc.data().dateTo ? (snap.payload.doc.data().dateTo as any).toDate() : null
                        })
                    );
                })
            );
    }

    public uploadFromJson(jsonTracks: Partial<Track>[]): Promise<void> {
        const batch = this.db.firestore.batch();

        jsonTracks.forEach(track => {
            const trackId = this.sharedService.generateFirestoreId();
            const docRef = this.db.collection(this.TRACKS_COLLECTION).doc(trackId).ref;
            batch.set(docRef, {
                id: trackId || null,
                name: track.name || null,
                address: track.address || null,
                zip: track.zip || null,
                city: track.city || null,
                country: track.country || null,
                homepage: track.homepage || null,
                length: track.length || null,
                width: track.width || null,
                drivingDirection: track.drivingDirection || null,
                numberOfTurns: track.numberOfTurns || null,
                layoutImgUrl: track.layoutImgUrl || null,
                dBLimit: track.dBLimit || null,
                description: track.description || null
            });
        });

        return batch.commit();
    }
}
