import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { TracksPageRoutingModule } from './tracks-routing.module';
import { TracksPage } from './tracks.page';
import { TrackItemComponent } from './track-item/track-item.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TracksPageRoutingModule,
        SharedModule,
        ReactiveFormsModule
    ],
  declarations: [TracksPage, TrackItemComponent],
  exports: [TrackItemComponent]
})
export class TracksPageModule { }
