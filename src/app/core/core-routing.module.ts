import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorePage } from './core.page';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/core/events',
    pathMatch: 'full'
  },
  {
    path: '',
    component: CorePage,
    children: [
      {
        path: 'events',
        loadChildren: () => import('./events/events.module').then(m => m.EventsPageModule)
      },
      {
        path: 'tracks',
        loadChildren: () => import('./tracks/tracks.module').then(m => m.TracksPageModule)
      },
      {
        path: 'messenger',
        loadChildren: () => import('./messenger/messenger.module').then(m => m.MessengerPageModule)
      },
      {
        path: 'promoters',
        loadChildren: () => import('./promoters/promoters.module').then(m => m.PromotersPageModule)
      },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then(m => m.ProfilePageModule),
        canLoad: [AuthGuard],
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorePageRoutingModule { }
