import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '../event.service';
import { AlertController, NavController } from '@ionic/angular';
import { Event } from '../event.model';
import { BookingService } from '../../../booking/booking.service';
import { DetailPageComponent } from '../../../shared/components/classes/detail-page.component';
import { Observable } from 'rxjs';
import { AppState } from '../../../app.module';
import { Store } from '@ngrx/store';
import { eventsGetEventAction } from '../../../state-management/events/events.actions';
import { selectEventsStateSelectedEvent } from '../../../state-management/events/events.selectors';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-event-detail',
    templateUrl: './event-detail.page.html',
    styleUrls: ['./event-detail.page.scss'],
})
export class EventDetailPage extends DetailPageComponent implements OnInit {

    public isLoading = false;
    public event$: Observable<Event>;

    constructor(
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        private route: ActivatedRoute,
        private eventService: EventService,
        private bookingService: BookingService,
        private store: Store<AppState>
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            await this.navigateBack();
            return;
        }

        this.isLoading = true;
        this.event$ = this.store.select(selectEventsStateSelectedEvent).pipe(
            tap(event => {
                if (event) {
                    this.isLoading = false;
                }
            })
        );
    }

    public onBookEvent() {
        this.bookingService.checkIfUserProfileIsComplete(this.route.snapshot.params.id).subscribe(e => console.log(e));
    }
}
