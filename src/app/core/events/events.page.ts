import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import { Event } from './event.model';
import { EventService } from './event.service';

import { PickerController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { selectEventsFilterHasFilter } from '../../state-management/eventsFilter/events-filter.selectors';
import {
    eventsFilterResetAllFiltersAction,
    eventsFilterSetAllFiltersAction
} from '../../state-management/eventsFilter/events-filter.actions';
import { CountryCodes, CountryFilterSelect } from '../../shared/models/filter-select.model';


export enum EventsCategory {
    AMATEUR = 'AMATEUR',
    PRO = 'PRO'
}

@Component({
    selector: 'app-events',
    templateUrl: './events.page.html',
    styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

    @ViewChild('dateFrom') dateFromPicker;
    @ViewChild('dateTo') dateToPicker;


    public pickerMinDate = new Date(Date.now()).toISOString().split('T')[0];

    public hasFilter$: Observable<boolean>;
    public championShipHasFilter$: Observable<boolean>;

    public championshipCountryFilter: CountryCodes;
    public countryFilter: any;
    public championshipDateFromFilter: any;
    public championshipDateToFilter: any;
    public dateFromFilter: any;
    public dateToFilter: any;

    public EventsCategoryEnum = EventsCategory;
    public chosenEventCategory: EventsCategory = EventsCategory.AMATEUR;

    constructor(
        public events$: EventService,
        private store: Store<AppState>,
        private pickerCtrl: PickerController
    ) {
    }

    public ngOnInit() {
        this.events$.init();
        this.hasFilter$ = this.store.select(selectEventsFilterHasFilter);
    }

    public ionViewWillEnter() {
        this.events$.init();
    }

    public onOpenDateFrom() {
        this.dateFromPicker.open();
    }

    public onOpenDateTo() {
        this.dateToPicker.open();
    }

    public onDatePicked() {
        this.store.dispatch(eventsFilterSetAllFiltersAction({
            filters: {
                dateFrom: {
                    name: 'dateFrom',
                    operation: '>=',
                    value: this.dateFromFilter ? new Date(this.dateFromFilter) : null
                },
                dateTo: {
                    name: 'dateFrom',
                    operation: '<=',
                    value: this.dateToFilter ? new Date(this.dateToFilter) : null
                }
            }
        }));
        this.events$.changeFilter();
    }

    public onClearFilter() {
        this.dateFromFilter = null;
        this.dateToFilter = null;
        this.countryFilter = null;
        this.store.dispatch(eventsFilterResetAllFiltersAction());
        this.events$.changeFilter();
    }

    public trackByEventId(_, event: Event) {
        return event.id;
    }

    private createCountryPickerColumns() {
        return CountryFilterSelect.map(c => ({text: c.selectorText, value: c.key}));
    }

    public async onOpenCountrySelector() {
        const el = await this.pickerCtrl.create({
            columns: [{name: 'country', options: this.createCountryPickerColumns()}],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Confirm',
                    handler: (value) => {
                        this.countryFilter = value.country.value;
                        this.onCountrySelected();
                    }
                }
            ]
        });
        await el.present();
    }

    public onCountrySelected() {
        this.store.dispatch(eventsFilterSetAllFiltersAction({
            filters: {
                country: {
                    name: 'track.country',
                    operation: '==',
                    value: this.countryFilter ? this.countryFilter : null
                }
            }
        }));
        this.events$.changeFilter();
    }
}
