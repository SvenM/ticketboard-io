import { CountryCodes } from '../../shared/models/filter-select.model';
import { TrainingType } from '../../state-management/eventsFilter/events-filter.reducer';

export interface Championship {
    id: string;
    logoUrl: string;
    track: string;
    startDate: Date;
    endDate: Date;
    country: CountryCodes;
    promoter: string;
    eventType: TrainingType;
    eventLink: string;
}

export interface ChampionshipJson {
    logoUrl: string;
    track: string;
    startDate: string;
    endDate: string;
    countryCode: CountryCodes,
    promoterName: string;
    eventType: string;
    eventsLink: string;
}
