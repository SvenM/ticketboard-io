import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Championship, ChampionshipJson } from './championship.model';
import { Observable } from 'rxjs';
import { map, scan, take, tap } from 'rxjs/operators';
import { SharedService } from '../../shared/shared.service';
import { TrainingType } from '../../state-management/eventsFilter/events-filter.reducer';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { selectChampionshipFiltersFilters } from '../../state-management/championshipFilter/championship-filter.selectors';
import { InfiniteLoopService } from '../../shared/services/infinite-loop.service';


@Injectable({
    providedIn: 'root'
})
export class ChampionshipService extends InfiniteLoopService {

    private CHAMPIONSHIP_COLLECTION = 'championships';

    public data: Observable<Partial<Championship>[]>;
    public done: Observable<boolean> = this._done.asObservable();
    public loading: Observable<boolean> = this._loading.asObservable();

    constructor(
        private db: AngularFirestore,
        private sharedService: SharedService,
        private store: Store<AppState>,
    ) {
        super();

        this.query = this.db
            .collection<Partial<Championship>[]>(this.CHAMPIONSHIP_COLLECTION)
            .ref
            .limit(15)
            .orderBy('startDate')
            .where('startDate', '>=', this.sharedService.createDateFrom(new Date(Date.now())));


    }

    public addChampionship(championship: Partial<Championship>): Promise<DocumentReference> {
        return this.db.collection(this.CHAMPIONSHIP_COLLECTION)
            .add(championship);
    }

    public init() {
        const first = this.db.collection(this.CHAMPIONSHIP_COLLECTION, ref => this.query);

        this._data.next([]);
        this._done.next(false);
        this.mapAndUpdate(first, this.updateCallback);

        this.data = this._data.asObservable().pipe(scan((acc, val) => {
            return acc.concat(val);
        }));
    }

    public more() {
        const cursor = this.getCursor();

        const more = this.db.collection(this.CHAMPIONSHIP_COLLECTION, ref => this.query.startAfter(cursor));
        this.mapAndUpdate(more, this.updateCallback);
    }

    private updateCallback = snap => {
        const data = {
            id: snap.payload.doc.id,
            ...snap.payload.doc.data(),
            startDate: snap.payload.doc.data().startDate ? (snap.payload.doc.data().startDate as any).toDate() : null,
            endDate: snap.payload.doc.data().endDate ? (snap.payload.doc.data().endDate as any).toDate() : null
        };
        const doc = snap.payload.doc;
        return {...data, doc};
    }

    public changeFilter(): void {
        this.store.select(selectChampionshipFiltersFilters).pipe(
            tap(filters => {
                let query = this.db
                    .collection<Partial<Championship>[]>(this.CHAMPIONSHIP_COLLECTION)
                    .ref
                    .limit(15)
                    .orderBy('startDate')
                    .where('startDate', '>=', this.sharedService.createDateFrom(new Date(Date.now())));

                if (filters) {
                    filters.forEach(filter => {
                        if (filter && filter.value) {
                            query = query.where(filter.name, filter.operation, filter.value);
                        }
                    });
                }

                this.query = query;
                this._data.next([]);
                this.init();
            }),
            take(1)
        ).subscribe();
    }

    public getChampionship(championshipId: string): Observable<Partial<Championship>> {
        return this.db.collection(this.CHAMPIONSHIP_COLLECTION).doc(championshipId).get()
            .pipe(
                take(1),
                map(championship => {
                    const data = championship.data();
                    return {
                        id: championship.id,
                        ...data,
                        startDate: data.startDate ? (data.startDate as any).toDate() : null,
                        endDate: data.endDate ? (data.endDate as any).toDate() : null
                    };
                })
            );
    }

    public getAllChampionshipsWithoutFilter(): Observable<Partial<Championship>[]> {
        const query = this.db
            .collection<Partial<Championship>[]>(this.CHAMPIONSHIP_COLLECTION)
            .ref
            .orderBy('startDate')
            .where('startDate', '>=', this.sharedService.createDateFrom(new Date(Date.now())));

        return this.db
            .collection<Partial<Championship>>(this.CHAMPIONSHIP_COLLECTION,
                _ => query)
            .valueChanges({idField: 'id'})
            .pipe(
                map(championships => this.mapChampionshipDocumentToData(championships))
            );

    }

    public deleteChampionship(championshipId: string): Promise<void> {
        return this.db.collection(this.CHAMPIONSHIP_COLLECTION).doc(championshipId).delete();
    }

    public updateChampionship(championshipId: string, data: Partial<Championship>): Promise<void> {
        return this.db.collection(this.CHAMPIONSHIP_COLLECTION).doc(championshipId).update(data);
    }

    public async uploadFromJson(jsonChampionships: Partial<ChampionshipJson>[]) {
        const uploads = jsonChampionships.map(async (championship) => {
            const championshipId = this.sharedService.generateFirestoreId();
            const championshipRef = this.db.collection(this.CHAMPIONSHIP_COLLECTION).doc<Championship>(championshipId);
            const championshipObject: Championship = {
                id: championshipId || null,
                logoUrl: null,
                track: championship.track || null,
                startDate: new Date(this.sharedService.createDateFrom(this.sharedService.createDateFromGermanDateString(championship.startDate))) || null,
                endDate: new Date(this.sharedService.createDateTo(this.sharedService.createDateFromGermanDateString(championship.endDate))) || null,
                country: championship.countryCode || null,
                promoter: championship.promoterName || null,
                eventType: championship.eventType as TrainingType || null,
                eventLink: championship.eventsLink || null
            };

            return {championshipRef, championshipObject};
        });

        const uploadObjects = await Promise.all(uploads);
        return uploadObjects.map(({championshipRef, championshipObject}) => {
            if (championshipRef && championshipObject) {
                const batch = this.db.firestore.batch();
                batch.set(championshipRef.ref, championshipObject);
                batch.commit();
            }
        });
    }

    private mapChampionshipDocumentToData(championships): Championship[] {
        return championships.map(c => {
            return {...c, startDate: (c.startDate as any).toDate(), endDate: (c.endDate as any).toDate()};
        });
    }
}
