import { Component, Input } from '@angular/core';

import { Event } from '../event.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { eventsGetEventAction } from '../../../state-management/events/events.actions';

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss'],
})
export class EventItemComponent {

  @Input() data: Event;

  constructor(private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute) { }

  public async onShowDetails() {
    await this.store.dispatch(eventsGetEventAction({id: this.data.id}));
    await this.router.navigate(['event', this.data.id], {relativeTo: this.activatedRoute});
  }

}
