import { Injectable } from '@angular/core';
import { Event, EventJson } from './event.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { take, map, scan, tap } from 'rxjs/operators';
import { SharedService } from 'src/app/shared/shared.service';
import { Promoter } from '../promoters/promoter.model';
import { Track } from '../tracks/track.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { selectEventFiltersFilters } from '../../state-management/eventsFilter/events-filter.selectors';
import { InfiniteLoopService } from '../../shared/services/infinite-loop.service';

@Injectable({
    providedIn: 'root'
})
export class EventService extends InfiniteLoopService {

    private EVENTS_COLLECTION = 'events';
    private PROMOTERS_COLLECTION = 'promoters';
    private TRACKS_COLLECTION = 'tracks';

    public data: Observable<Partial<Event>[]>;
    public done: Observable<boolean> = this._done.asObservable();
    public loading: Observable<boolean> = this._loading.asObservable();


    constructor(
        private db: AngularFirestore,
        private sharedService: SharedService,
        private store: Store<AppState>
    ) {
        super();

        this.query = this.db
            .collection<Partial<Event>[]>(this.EVENTS_COLLECTION)
            .ref
            .limit(15)
            .orderBy('dateFrom')
            .where('dateFrom', '>=', this.sharedService.createDateFrom(new Date(Date.now())));
    }

    public init() {
        const first = this.db.collection(this.EVENTS_COLLECTION, ref => this.query);

        this._data.next([]);
        this._done.next(false);
        this.mapAndUpdate(first, this.updateCallback);

        this.data = this._data.asObservable().pipe(scan((acc, val) => {
            return acc.concat(val);
        }));
    }

    public more() {
        const cursor = this.getCursor();

        const more = this.db.collection(this.EVENTS_COLLECTION, ref => this.query.startAfter(cursor));
        this.mapAndUpdate(more, this.updateCallback);
    }

    private updateCallback = snap => {
        const data = {
            id: snap.payload.doc.id,
            ...snap.payload.doc.data(),
            dateFrom: snap.payload.doc.data().dateFrom ? (snap.payload.doc.data().dateFrom as any).toDate() : null,
            dateTo: snap.payload.doc.data().dateTo ? (snap.payload.doc.data().dateTo as any).toDate() : null
        };
        const doc = snap.payload.doc;
        return {...data, doc};
    }

    public changeFilter(): void {
        this.store.select(selectEventFiltersFilters).pipe(
            tap(filters => {
                let query = this.db
                    .collection<Partial<Event>[]>(this.EVENTS_COLLECTION)
                    .ref
                    .limit(15)
                    .orderBy('dateFrom')
                    .where('dateFrom', '>=', this.sharedService.createDateFrom(new Date(Date.now())));

                if (filters) {
                    filters.forEach(filter => {
                        if (filter.value) {
                            query = query.where(filter.name, filter.operation, filter.value);
                        }
                    });
                }

                this.query = query;
                this.init();
            }),
            take(1)
        ).subscribe();
    }

    public async addEvent(event: Partial<Event>): Promise<void> {
        const ref = await this.db
            .collection(this.EVENTS_COLLECTION)
            .add(event);

        const batch = this.db.firestore.batch();
        const promotersRef = this.db
            .collection(this.PROMOTERS_COLLECTION)
            .doc(event.promoter.id)
            .collection(this.EVENTS_COLLECTION)
            .doc(ref.id).ref;
        const tracksRef = this.db
            .collection(this.TRACKS_COLLECTION)
            .doc(event.track.id)
            .collection(this.EVENTS_COLLECTION)
            .doc(ref.id).ref;
        batch.set(promotersRef, event);
        batch.set(tracksRef, event);
        await batch.commit();

    }

    public getEvent(eventId: string): Promise<Partial<Event>> {
        return this.db
            .collection<Partial<Event>>(this.EVENTS_COLLECTION)
            .doc(eventId)
            .get()
            .pipe(
                take(1),
                map(event => {
                    const data = event.data();
                    return {
                        id: eventId,
                        ...data,
                        dateFrom: data.dateFrom ? (data.dateFrom as any).toDate() : null,
                        dateTo: data.dateTo ? (data.dateTo as any).toDate() : null
                    };
                })
            ).toPromise();
    }

    public getAllEventsWithoutFilter(): Observable<Partial<Event>[]> {
        const query = this.db
            .collection<Partial<Event>[]>(this.EVENTS_COLLECTION)
            .ref
            .orderBy('dateFrom')
            .where('dateFrom', '>=', this.sharedService.createDateFrom(new Date(Date.now())));

        return this.db
            .collection<Partial<Event>[]>(this.EVENTS_COLLECTION,
                ref => query)
            .valueChanges({idField: 'id'}).pipe(
                map(events => {
                    return this.mapEventDocumentToData(events);
                })
            );
    }

    public deleteEvent(eventId: string): Promise<void> {
        const eventRef = this.db
            .collection(this.EVENTS_COLLECTION)
            .doc(eventId).ref;
        return this.db.firestore.runTransaction(async t => {
            const doc = await t.get(eventRef);
            const batch = this.db.firestore.batch();
            const promotersRef = this.db
                .collection(this.PROMOTERS_COLLECTION)
                .doc(doc.data().promoter.id)
                .collection(this.EVENTS_COLLECTION)
                .doc(doc.id).ref;
            const tracksRef = this.db
                .collection(this.TRACKS_COLLECTION)
                .doc(doc.data().track.id)
                .collection(this.EVENTS_COLLECTION)
                .doc(doc.id).ref;
            batch.delete(promotersRef);
            batch.delete(tracksRef);
            await batch.commit();
            t.delete(eventRef);
            return Promise.resolve();
        });

    }

    public updateEvent(eventId: string, data: Partial<Event>): Promise<void> {
        const eventRef = this.db
            .collection(this.EVENTS_COLLECTION)
            .doc(eventId).ref;
        return this.db.firestore.runTransaction(async t => {
            const doc = await t.get(eventRef);
            const batch = this.db.firestore.batch();
            const promotersRef = this.db
                .collection(this.PROMOTERS_COLLECTION)
                .doc(doc.data().promoter.id)
                .collection(this.EVENTS_COLLECTION)
                .doc(doc.id).ref;
            const tracksRef = this.db
                .collection(this.TRACKS_COLLECTION)
                .doc(doc.data().track.id)
                .collection(this.EVENTS_COLLECTION)
                .doc(doc.id).ref;
            batch.set(promotersRef, data);
            batch.set(tracksRef, data);
            await batch.commit();
            t.update(eventRef, data);
            return Promise.resolve();
        });
    }

    public async uploadFromJson(jsonEvents: Partial<EventJson>[]): Promise<void | void[]> {
        const uploads = await jsonEvents.map(async (event, index) => {
            console.log(event);
            console.log(event.promoterName);
            console.log(event.trackName);
            const promoter = await this.db
                .collection<Promoter>(this.PROMOTERS_COLLECTION, ref => ref.where('name', '==', event.promoterName))
                .doc().get().toPromise();
            const track = await this.db
                .collection<Track>(this.TRACKS_COLLECTION, ref => ref.where('name', '==', event.trackName))
                .doc().get().toPromise();

            console.log(promoter);
            console.log(track);

            if (!promoter) {
                console.log(`Error in JSON Object #${ index + 1 }. No promoter named ${ event.promoterName } found in database.`);
                return;
            }

            if (!track) {
                console.log(`Error in JSON Object #${ index + 1 }. No track named ${ event.trackName } found in database.`);
                return;
            }

            const eventId = this.sharedService.generateFirestoreId();
            const eventRef = this.db.collection(this.EVENTS_COLLECTION).doc<Event>(eventId);
            const eventObject: Event = {
                catering: event.catering || null,
                country: event.countryCode || null,
                dateFrom: new Date(this.sharedService.createDateFrom(this.sharedService.createDateFromGermanDateString(event.startDate))) || null,
                dateTo: new Date(this.sharedService.createDateTo(this.sharedService.createDateFromGermanDateString(event.endDate))) || null,
                difficulty: event.difficulty || null, // deprecated
                id: eventId || null,
                instructions: event.instructions || null,
                photoService: event.photoService || null,
                price: event.price || null,
                promoter: promoter.data() as Promoter || null,
                races: event.races || null,
                service: event.service || null,
                timekeeping: event.timekeeping || null,
                track: track.data() as Track || null,
                trainingType: event.raceType || null,
                eventLink: event.eventsLink || null
            };

            console.log(eventObject);

            return {eventRef, eventObject};
        });


        const uploadObjects = await Promise.all(uploads);
        return uploadObjects.map(({eventRef, eventObject}) => {
            if (eventRef && eventObject) {
                const batch = this.db.firestore.batch();
                batch.set(eventRef.ref, eventObject);
                const promotersRef = this.db
                    .collection(this.PROMOTERS_COLLECTION)
                    .doc(eventObject.promoter.id)
                    .collection(this.EVENTS_COLLECTION)
                    .doc(eventObject.id).ref;
                const tracksRef = this.db
                    .collection(this.TRACKS_COLLECTION)
                    .doc(eventObject.track.id)
                    .collection(this.EVENTS_COLLECTION)
                    .doc(eventObject.id).ref;
                batch.set(promotersRef, eventObject);
                batch.set(tracksRef, eventObject);
                batch.commit();
            }
        });
    }

    private mapEventDocumentToData(events) {
        return events.map(event => {
            return {...event, dateFrom: (event.dateFrom as any).toDate(), dateTo: (event.dateTo as any).toDate()};
        });
    }
}
