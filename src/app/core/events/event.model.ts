import { Track } from '../tracks/track.model';
import { Promoter } from '../promoters/promoter.model';
import { CountryCodes } from '../../shared/models/filter-select.model';
import { BikeType, TrainingType } from '../../state-management/eventsFilter/events-filter.reducer';


export interface Event {
    id: string;
    country: CountryCodes;
    dateFrom: Date;
    dateTo: Date;
    promoter: Promoter;
    price: number;
    track: Track;
    trainingType: TrainingType;
    difficulty: 'easy' | 'medium' | 'hard';
    catering: boolean;
    instructions: boolean;
    photoService: boolean;
    races: boolean;
    service: boolean;
    timekeeping: boolean;
    eventLink: string;
}

export interface EventJson {
    bikeType: BikeType;
    catering: boolean;
    countryCode: CountryCodes;
    endDate: string;
    eventsLink: string;
    instructions: boolean;
    logo: string;
    photoService: boolean;
    price: number;
    promoterName: string;
    races: boolean;
    raceType: TrainingType;
    service: boolean;
    startDate: string;
    timekeeping: boolean;
    trackName: string;
    difficulty: 'easy' | 'medium' | 'hard';
}
