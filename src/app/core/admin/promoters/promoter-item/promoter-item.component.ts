import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Promoter } from 'src/app/core/promoters/promoter.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../app.module';
import { promotersDeletePromoterAction, promotersGetPromoterAction } from '../../../../state-management/promoters/promoters.actions';

@Component({
  selector: 'app-promoter-item',
  templateUrl: './promoter-item.component.html',
  styleUrls: ['./promoter-item.component.scss'],
})
export class PromoterItemComponent {

  @Input() data: Partial<Promoter>;

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute
  ) { }

  public async onEdit() {
    await this.store.dispatch(promotersGetPromoterAction({id: this.data.id}));
    await this.router.navigate(['edit', this.data.id], {relativeTo: this.activatedRoute});
  }

  public async onDelete() {
    this.store.dispatch(promotersDeletePromoterAction({id: this.data.id}));
  }
}
