import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';

import { PromoterService } from '../../../promoters/promoter.service';
import { SharedService } from 'src/app/shared/shared.service';
import { UploadImageService } from 'src/app/shared/upload-image.service';

@Component({
    selector: 'app-new-promoter',
    templateUrl: './new-promoter.page.html',
    styleUrls: ['./new-promoter.page.scss'],
})
export class NewPromoterPage implements OnInit {

    public form: FormGroup;

    constructor(
        private loaderCtrl: LoadingController,
        private router: Router,
        private promoterService: PromoterService,
        private sharedService: SharedService,
        private uploadImageService: UploadImageService,
        private alertCtrl: AlertController,
    ) {
    }

    public ngOnInit() {

        this.form = new FormGroup({
            name: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            homepage: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            contact: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            address: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            zip: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            city: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            telephone: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            email: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            logoUrl: new FormControl(null)
        });
    }

    public async onCreatePromoter() {
        if (!this.form.valid) {
            return;
        }

        await this.promoterService.addPromoter({
            name: this.form.value.name,
            homepage: this.form.value.homepage,
            contact: this.form.value.contact,
            address: this.form.value.address,
            zip: this.form.value.zip,
            city: this.form.value.city,
            telephone: this.form.value.telephone,
            email: this.form.value.email,
            logoUrl: this.form.value.logoUrl
        }).catch(async err => {
            const alertEl = await this.alertCtrl.create({header: 'Error', message: err, buttons: ['Okay']});
            await alertEl.present();

            return;
        });

        this.form.reset();
        await this.router.navigate(['/core/admin/promoters']);
    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'logoUrl');
    }
}
