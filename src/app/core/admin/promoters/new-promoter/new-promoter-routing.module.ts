import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewPromoterPage } from './new-promoter.page';

const routes: Routes = [
  {
    path: '',
    component: NewPromoterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewPromoterPageRoutingModule { }
