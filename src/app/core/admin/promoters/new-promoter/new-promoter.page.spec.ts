import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewPromoterPage } from './new-promoter.page';

describe('NewPromoterPage', () => {
  let component: NewPromoterPage;
  let fixture: ComponentFixture<NewPromoterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewPromoterPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewPromoterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
