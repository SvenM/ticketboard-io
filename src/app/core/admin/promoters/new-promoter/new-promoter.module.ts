import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from 'src/app/shared/shared.module';
import { NewPromoterPageRoutingModule } from './new-promoter-routing.module';
import { NewPromoterPage } from './new-promoter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewPromoterPageRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [NewPromoterPage]
})
export class NewPromoterPageModule { }
