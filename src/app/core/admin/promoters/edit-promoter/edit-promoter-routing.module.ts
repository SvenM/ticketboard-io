import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPromoterPage } from './edit-promoter.page';

const routes: Routes = [
  {
    path: '',
    component: EditPromoterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPromoterPageRoutingModule { }
