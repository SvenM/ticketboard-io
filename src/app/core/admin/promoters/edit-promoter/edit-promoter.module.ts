import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from 'src/app/shared/shared.module';
import { EditPromoterPageRoutingModule } from './edit-promoter-routing.module';
import { EditPromoterPage } from './edit-promoter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPromoterPageRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [EditPromoterPage]
})
export class EditPromoterPageModule { }
