import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { tap } from 'rxjs/operators';

import { PromoterService } from 'src/app/core/promoters/promoter.service';
import { SharedService } from 'src/app/shared/shared.service';
import { UploadImageService } from 'src/app/shared/upload-image.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../app.module';
import { DetailPageComponent } from '../../../../shared/components/classes/detail-page.component';
import { PromotersState } from '../../../../state-management/promoters/promoters.reducer';
import { selectPromotersState } from '../../../../state-management/promoters/promoters.selectors';


@Component({
    selector: 'app-edit-promoter',
    templateUrl: './edit-promoter.page.html',
    styleUrls: ['./edit-promoter.page.scss'],
})
export class EditPromoterPage extends DetailPageComponent implements OnInit {

    public form: FormGroup;

    public state$: Observable<PromotersState>;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private promoterService: PromoterService,
        private loaderCtrl: LoadingController,
        private sharedService: SharedService,
        private uploadImageService: UploadImageService,
        private store: Store<AppState>
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        this.state$ = this.store.select(selectPromotersState).pipe(
            tap(state => {
                if(!state) {
                    this.navCtrl.back();
                }
                this.form = new FormGroup({
                    name: new FormControl(state.selectedPromoter.name, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    homepage: new FormControl(state.selectedPromoter.homepage, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    contact: new FormControl(state.selectedPromoter.contact, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    address: new FormControl(state.selectedPromoter.address, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    zip: new FormControl(state.selectedPromoter.zip, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    city: new FormControl(state.selectedPromoter.city, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    telephone: new FormControl(state.selectedPromoter.telephone, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    email: new FormControl(state.selectedPromoter.email, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    logoUrl: new FormControl(state.selectedPromoter.logoUrl)
                });
            })
        );
    }

    public async onUpdatePromoter() {
        const id = this.route.snapshot.params.promoterId;


        await this.promoterService.updatePromoter(id, {
            name: this.form.value.name,
            homepage: this.form.value.homepage,
            contact: this.form.value.contact,
            address: this.form.value.address,
            zip: this.form.value.zip,
            city: this.form.value.city,
            telephone: this.form.value.telephone,
            email: this.form.value.email,
            logoUrl: this.form.value.logoUrl
        });

        await this.navigateBack();
    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'logoUrl');
    }

}
