import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditPromoterPage } from './edit-promoter.page';

describe('EditPromoterPage', () => {
  let component: EditPromoterPage;
  let fixture: ComponentFixture<EditPromoterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditPromoterPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditPromoterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
