import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PromoterListPageRoutingModule } from './promoter-list-routing.module';
import { PromoterListPage } from './promoter-list.page';
import { PromoterItemComponent } from '../promoter-item/promoter-item.component';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PromoterListPageRoutingModule,
        SharedModule
    ],
    declarations: [PromoterListPage, PromoterItemComponent],
    exports: [PromoterItemComponent]
})
export class PromoterListPageModule {
}
