import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromoterListPage } from './promoter-list.page';

const routes: Routes = [
  {
    path: '',
    component: PromoterListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromoterListPageRoutingModule { }
