import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Promoter } from 'src/app/core/promoters/promoter.model';
import { PromoterService } from 'src/app/core/promoters/promoter.service';

@Component({
    selector: 'app-promoter-list',
    templateUrl: './promoter-list.page.html',
    styleUrls: ['./promoter-list.page.scss'],
})
export class PromoterListPage implements OnInit {

    public promoters$: Observable<Partial<Promoter>[]>;

    constructor(
        private promoterService: PromoterService) {
    }

    public ngOnInit() {
        this.promoters$ = this.promoterService.getAllPromoters()
    }

    onJsonPicked(event) {
        this.promoterService.uploadFromJson(event);
    }


}
