import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromoterListPage } from './promoter-list.page';

describe('PromoterListPage', () => {
  let component: PromoterListPage;
  let fixture: ComponentFixture<PromoterListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PromoterListPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromoterListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
