import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessPageRoutingModule } from './business-routing.module';

import { BusinessPage } from './business.page';
import { BusinessAccountItemComponent } from './business-account-item/business-account-item.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        BusinessPageRoutingModule,
        SharedModule
    ],
    declarations: [BusinessPage, BusinessAccountItemComponent],
  exports: [BusinessAccountItemComponent]
})
export class BusinessPageModule {}
