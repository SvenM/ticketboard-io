import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BusinessService } from '../../../business/business.service';

@Component({
  selector: 'tb-business',
  templateUrl: './business.page.html',
  styleUrls: ['./business.page.scss'],
})
export class BusinessPage implements OnInit {

  public businesses$: Observable<any>;

  constructor(private businessService: BusinessService) { }

  ngOnInit() {
    this.businesses$ = this.businessService.getAllBusinessAccounts();
  }

}
