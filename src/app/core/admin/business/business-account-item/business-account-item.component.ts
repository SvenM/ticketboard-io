import { Component, Input } from '@angular/core';
import { BusinessProfile } from '../../../../business/business-profile';
import { BusinessService } from '../../../../business/business.service';
import { ModalController } from '@ionic/angular';
import { AssignPromoterModalPage } from '../assign-promoter-modal/assign-promoter-modal.page';

@Component({
    selector: 'tb-business-account-item',
    templateUrl: './business-account-item.component.html',
    styleUrls: ['./business-account-item.component.scss'],
})
export class BusinessAccountItemComponent {

    @Input() public data: BusinessProfile;

    constructor(private businessService: BusinessService, private modalCtrl: ModalController) {
    }


    public onApprove() {
        if (this.data) {
            this.businessService.approveAccount(this.data.id, !this.data.isApproved);
        }
    }

    public async onAssignPromoter() {
        const modal = await this.modalCtrl.create({
            component: AssignPromoterModalPage,
            componentProps: {
                accountId: this.data.id
            }
        });
        await modal.present();
    }

    public onRemovePromoter() {
        this.businessService.removePromoterId(this.data.id);
    }
}
