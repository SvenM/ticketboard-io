import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessPage } from './business.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessPage
  },
  {
    path: 'assign-promoter-modal',
    loadChildren: () => import('./assign-promoter-modal/assign-promoter-modal.module').then( m => m.AssignPromoterModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessPageRoutingModule {}
