import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BusinessService } from '../../../../business/business.service';
import { PromoterService } from '../../../promoters/promoter.service';

@Component({
    selector: 'tb-assign-promoter-modal',
    templateUrl: './assign-promoter-modal.page.html',
    styleUrls: ['./assign-promoter-modal.page.scss'],
})
export class AssignPromoterModalPage implements OnInit {

    @Input() public accountId: string;

    public form: FormGroup;

    constructor(
        private modalCtrl: ModalController,
        private businessService: BusinessService,
        private promoterService: PromoterService) {
    }

    public ngOnInit() {
        this.form = new FormGroup({
            promoter: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            })
        });
    }


    public async onDismissModal() {
        await this.modalCtrl.dismiss();
    }

    public async onSubmit() {
        if (!this.form.valid) {
            return;
        }

        await this.businessService.assignPromoterId(this.accountId, this.form.value.promoter.id);
        await this.onDismissModal();
    }

    public async onAssignNewPromoterProfile() {
        const docReference = await this.promoterService.createEmptyPromoter();
        await this.businessService.assignPromoterId(this.accountId, docReference.id);
        await this.onDismissModal();
    }
}
