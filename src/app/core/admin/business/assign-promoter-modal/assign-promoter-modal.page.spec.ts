import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssignPromoterModalPage } from './assign-promoter-modal.page';

describe('AssignPromoterModalPage', () => {
  let component: AssignPromoterModalPage;
  let fixture: ComponentFixture<AssignPromoterModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignPromoterModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AssignPromoterModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
