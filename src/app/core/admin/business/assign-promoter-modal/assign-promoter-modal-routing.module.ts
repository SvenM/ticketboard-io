import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssignPromoterModalPage } from './assign-promoter-modal.page';

const routes: Routes = [
  {
    path: '',
    component: AssignPromoterModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignPromoterModalPageRoutingModule {}
