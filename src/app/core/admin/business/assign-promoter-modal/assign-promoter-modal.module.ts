import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssignPromoterModalPageRoutingModule } from './assign-promoter-modal-routing.module';

import { AssignPromoterModalPage } from './assign-promoter-modal.page';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AssignPromoterModalPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [AssignPromoterModalPage]
})
export class AssignPromoterModalPageModule {
}
