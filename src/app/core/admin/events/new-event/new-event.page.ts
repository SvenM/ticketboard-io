import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';

import { EventService } from 'src/app/core/events/event.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
    selector: 'app-new-event',
    templateUrl: './new-event.page.html',
    styleUrls: ['./new-event.page.scss'],
})
export class NewEventPage implements OnInit {
    public form: FormGroup;

    constructor(
        private loaderCtrl: LoadingController,
        private router: Router,
        private eventService: EventService,
        private sharedService: SharedService,
        private alertCtrl: AlertController
    ) {
    }

    public ngOnInit() {
        this.form = new FormGroup({
            promoter: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            dateFrom: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            dateTo: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            track: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            price: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            catering: new FormControl(false, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            instructions: new FormControl(false, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            races: new FormControl(false, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            photoService: new FormControl(false, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            service: new FormControl(false, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            timekeeping: new FormControl(false, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
        });
    }

    public async onCreateEvent() {
        if (!this.form.valid) {
            return;
        }

        await this.eventService.addEvent({
            promoter: this.form.value.promoter,
            dateFrom: this.sharedService.createDateFrom(this.form.value.dateFrom),
            dateTo: this.sharedService.createDateTo(this.form.value.dateTo),
            track: this.form.value.track,
            price: this.form.value.price,
            country: this.form.value.track.country,
            catering: this.form.value.catering,
            instructions: this.form.value.instructions,
            races: this.form.value.races,
            photoService: this.form.value.photoService,
            service: this.form.value.service,
            timekeeping: this.form.value.timekeeping
        }).catch(async err => {
            const alertEl = await this.alertCtrl.create({header: 'Error', message: err, buttons: ['Okay']});
            await alertEl.present();

            return;
        });

        this.form.reset();
        await this.router.navigate(['/core/admin/events']);
    }
}
