import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Event } from 'src/app/core/events/event.model';
import { AppState } from '../../../../app.module';
import { Store } from '@ngrx/store';
import { eventsDeleteEventAction, eventsGetEventAction } from '../../../../state-management/events/events.actions';



@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss'],
})
export class EventItemComponent {

  @Input() data: Partial<Event>;

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute
  ) { }

  public async onEdit() {
    await this.store.dispatch(eventsGetEventAction({id: this.data.id}));
    await this.router.navigate(['edit', this.data.id], {relativeTo: this.activatedRoute});
  }

  public onDelete() {
    this.store.dispatch(eventsDeleteEventAction({id: this.data.id}));
  }
}
