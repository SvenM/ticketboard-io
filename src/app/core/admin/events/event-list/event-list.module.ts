import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { EventListPageRoutingModule } from './event-list-routing.module';
import { EventListPage } from './event-list.page';
import { EventItemComponent } from '../event-item/event-item.component';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EventListPageRoutingModule,
        SharedModule
    ],
  declarations: [EventListPage, EventItemComponent],
  exports: [EventItemComponent]
})
export class EventListPageModule { }
