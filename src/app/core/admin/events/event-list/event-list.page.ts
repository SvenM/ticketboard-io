import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { Event } from 'src/app/core/events/event.model';
import { EventService } from 'src/app/core/events/event.service';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.page.html',
  styleUrls: ['./event-list.page.scss'],
})
export class EventListPage implements OnInit {

  public events$: Observable<Partial<Event>[]>;

  private unsubscribe: Subject<any>;

  constructor(private eventService: EventService) {
    this.unsubscribe = new Subject<any>();
  }

  public ngOnInit() {
    this.events$ = this.eventService.getAllEventsWithoutFilter()
  }


  public onJsonPicked(event) {
    this.eventService.uploadFromJson(event);
  }

}
