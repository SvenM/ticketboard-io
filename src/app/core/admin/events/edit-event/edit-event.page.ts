import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { EventService } from 'src/app/core/events/event.service';
import { SharedService } from 'src/app/shared/shared.service';
import { AppState } from '../../../../app.module';
import { Store } from '@ngrx/store';
import { DetailPageComponent } from '../../../../shared/components/classes/detail-page.component';
import { EventsState } from '../../../../state-management/events/events.reducer';
import { selectEventsState } from '../../../../state-management/events/events.selectors';


@Component({
    selector: 'app-edit-event',
    templateUrl: './edit-event.page.html',
    styleUrls: ['./edit-event.page.scss'],
})
export class EditEventPage extends DetailPageComponent implements OnInit {

    public form: FormGroup;

    public state$: Observable<EventsState>;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private eventService: EventService,
        private loaderCtrl: LoadingController,
        private sharedService: SharedService,
        private store: Store<AppState>
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        const id = this.route.snapshot.params.eventId;

        if (!id) {
            await this.navigateBack();
            return;
        }

        this.state$ = this.store.select(selectEventsState).pipe(tap(state => {
            if (!state) {
                this.navigateBack();
            }
            this.form = new FormGroup({
                promoter: new FormControl(state.selectedEvent.promoter, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                dateFrom: new FormControl(
                    state.selectedEvent.dateFrom ? new Date(state.selectedEvent.dateFrom).toUTCString() : null,
                    {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }
                ),
                dateTo: new FormControl(state.selectedEvent.dateTo ? new Date(state.selectedEvent.dateTo).toUTCString() : null,
                    {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }
                ),
                track: new FormControl(state.selectedEvent.track, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                price: new FormControl(state.selectedEvent.price, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                catering: new FormControl(state.selectedEvent.catering, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                instructions: new FormControl(state.selectedEvent.instructions, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                races: new FormControl(state.selectedEvent.races, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                photoService: new FormControl(state.selectedEvent.photoService, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                service: new FormControl(state.selectedEvent.service, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
                timekeeping: new FormControl(state.selectedEvent.timekeeping, {
                    updateOn: 'change',
                    validators: [Validators.required]
                }),
            });
        }));
    }

    public async onUpdateEvent() {
        if (!this.form.valid) {
            return;
        }

        const id = this.route.snapshot.params.eventId;

        await this.eventService.updateEvent(id, {
            promoter: this.form.value.promoter,
            dateFrom: this.form.value.dateFrom ? this.sharedService.createDateFrom(this.form.value.dateFrom) : null,
            dateTo: this.form.value.dateTo ? this.sharedService.createDateTo(this.form.value.dateTo) : null,
            track: this.form.value.track,
            price: this.form.value.price,
            country: this.form.value.track.country,
            catering: this.form.value.catering,
            instructions: this.form.value.instructions,
            races: this.form.value.races,
            photoService: this.form.value.photoService,
            service: this.form.value.service,
            timekeeping: this.form.value.timekeeping
        });

        this.sharedService.showSpinner('Updating...').then(() => {
            this.form.reset();
            this.navigateBack();
        });
    }
}
