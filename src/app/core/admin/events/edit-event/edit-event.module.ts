import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { EditEventPage } from './edit-event.page';
import { EditEventPageRoutingModule } from './edit-event-routing.module';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EditEventPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
  declarations: [EditEventPage]
})
export class EditEventPageModule { }
