import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Observable, of, from } from 'rxjs';
import { take, switchMap } from 'rxjs/operators';

import { Auth2Service } from 'src/app/auth/auth2.service';



@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private auth2Service: Auth2Service, private loaderCtrl: LoadingController) {

  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    const loader = from(this.loaderCtrl.create({
      message: 'Lade...'
    }));


    return this.auth2Service.isUserAdmin().pipe(
      take(1),
      switchMap(isAdmin => of(isAdmin)),
    );

  }
}
