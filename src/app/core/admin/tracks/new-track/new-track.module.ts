import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NewTrackPageRoutingModule } from './new-track-routing.module';
import { NewTrackPage } from './new-track.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewTrackPageRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [NewTrackPage]
})
export class NewTrackPageModule { }
