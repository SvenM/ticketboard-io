import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';

import { TrackService } from '../../../tracks/track.service';
import { SharedService } from 'src/app/shared/shared.service';
import { UploadImageService } from 'src/app/shared/upload-image.service';

@Component({
    selector: 'app-new-track',
    templateUrl: './new-track.page.html',
    styleUrls: ['./new-track.page.scss'],
})
export class NewTrackPage implements OnInit {

    public form: FormGroup;

    constructor(
        private loaderCtrl: LoadingController,
        private router: Router,
        private trackService: TrackService,
        private sharedService: SharedService,
        private uploadImageService: UploadImageService,
        private alertCtrl: AlertController,
    ) {
    }

    public ngOnInit() {
        this.form = new FormGroup({
            name: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            address: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            zip: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            city: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            country: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            homepage: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            length: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            width: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            drivingDirection: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            numberOfTurns: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            layoutImgUrl: new FormControl(null)
        });
    }

    public async onCreateTrack() {
        if (!this.form.valid) {
            return;
        }
        await this.trackService.addTrack({
            name: this.form.value.name,
            address: this.form.value.address,
            zip: this.form.value.zip,
            city: this.form.value.city,
            country: this.form.value.country,
            homepage: this.form.value.homepage,
            length: this.form.value.length,
            width: this.form.value.width,
            drivingDirection: this.form.value.drivingDirection,
            numberOfTurns: this.form.value.numberOfTurns,
            layoutImgUrl: this.form.value.layoutImgUrl
        }).catch(async err => {
            const alertEl = await this.alertCtrl.create({header: 'Error', message: err, buttons: ['Okay']});
            await alertEl.present();

            return;
        });

        this.form.reset();
        await this.router.navigate(['/core/admin/tracks']);

    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'layoutImgUrl');
    }
}
