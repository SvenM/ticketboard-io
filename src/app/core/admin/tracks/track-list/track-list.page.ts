import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Track } from 'src/app/core/tracks/track.model';
import { TrackService } from 'src/app/core/tracks/track.service';

@Component({
    selector: 'app-track-list',
    templateUrl: './track-list.page.html',
    styleUrls: ['./track-list.page.scss'],
})
export class TrackListPage implements OnInit {

    public tracks$: Observable<Partial<Track>[]>;

    constructor(
        private trackService: TrackService,
    ) {
    }

    public ngOnInit() {
        this.tracks$ = this.trackService.getAllTracks();
    }


    async onJsonPicked(event: Partial<Track>[]) {
        await this.trackService.uploadFromJson(event);
    }

}
