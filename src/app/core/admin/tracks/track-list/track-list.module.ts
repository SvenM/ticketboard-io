import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { TrackListPageRoutingModule } from './track-list-routing.module';
import { TrackListPage } from './track-list.page';
import { TrackItemComponent } from '../track-item/track-item.component';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TrackListPageRoutingModule,
        SharedModule
    ],
  declarations: [TrackListPage, TrackItemComponent],
  exports: [TrackItemComponent]
})
export class TrackListPageModule { }
