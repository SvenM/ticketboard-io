import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController, LoadingController } from '@ionic/angular';

import { TrackService } from 'src/app/core/tracks/track.service';
import { SharedService } from 'src/app/shared/shared.service';
import { UploadImageService } from 'src/app/shared/upload-image.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../app.module';
import { DetailPageComponent } from '../../../../shared/components/classes/detail-page.component';
import { TracksState } from '../../../../state-management/tracks/tracks.reducer';
import { selectTracksState } from '../../../../state-management/tracks/tracks.selectors';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-edit-track',
    templateUrl: './edit-track.page.html',
    styleUrls: ['./edit-track.page.scss'],
})
export class EditTrackPage extends DetailPageComponent implements OnInit {

    public form: FormGroup;
    public isLoading = false;

    public state$: Observable<TracksState>;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private trackService: TrackService,
        private loaderCtrl: LoadingController,
        private sharedService: SharedService,
        private uploadImageService: UploadImageService,
        private store: Store<AppState>
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        this.state$ = this.store.select(selectTracksState).pipe(tap(state => {
                if (!state) {
                    this.navCtrl.back();
                }
                this.form = new FormGroup({
                    name: new FormControl(state.selectedTrack.name, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    address: new FormControl(state.selectedTrack.address, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    zip: new FormControl(state.selectedTrack.zip, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    city: new FormControl(state.selectedTrack.city, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    country: new FormControl(state.selectedTrack.country, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    homepage: new FormControl(state.selectedTrack.homepage, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    length: new FormControl(state.selectedTrack.length, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    width: new FormControl(state.selectedTrack.width, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    drivingDirection: new FormControl(state.selectedTrack.drivingDirection, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    numberOfTurns: new FormControl(state.selectedTrack.numberOfTurns, {
                        updateOn: 'blur',
                        validators: [Validators.required]
                    }),
                    layoutImgUrl: new FormControl(state.selectedTrack.layoutImgUrl)
                });
            }
        ));
    }

    public async onUpdateTrack() {
        const id = this.route.snapshot.params.trackId;

        await this.trackService.updateTrack(id, {
            name: this.form.value.name,
            address: this.form.value.address,
            zip: this.form.value.zip,
            city: this.form.value.city,
            country: this.form.value.country,
            homepage: this.form.value.homepage,
            length: this.form.value.length,
            width: this.form.value.width,
            drivingDirection: this.form.value.drivingDirection,
            numberOfTurns: this.form.value.numberOfTurns,
            layoutImgUrl: this.form.value.layoutImgUrl
        });

        await this.navigateBack();
    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'layoutImgUrl');
    }
}
