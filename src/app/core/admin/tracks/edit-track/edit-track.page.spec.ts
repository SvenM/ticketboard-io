import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditTrackPage } from './edit-track.page';

describe('EditTrackPage', () => {
  let component: EditTrackPage;
  let fixture: ComponentFixture<EditTrackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTrackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditTrackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
