import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Track } from 'src/app/core/tracks/track.model';
import { TrackService } from 'src/app/core/tracks/track.service';
import { AppState } from '../../../../app.module';
import { Store } from '@ngrx/store';
import { tracksDeleteTrackAction, tracksGetTrackAction } from '../../../../state-management/tracks/tracks.actions';


@Component({
  selector: 'app-track-item',
  templateUrl: './track-item.component.html',
  styleUrls: ['./track-item.component.scss'],
})
export class TrackItemComponent {

  @Input() data: Partial<Track>;

  constructor(
    private router: Router,
    private trackService: TrackService,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute
  ) { }

  public async onEdit() {
    await this.store.dispatch(tracksGetTrackAction({id: this.data.id}));
    await this.router.navigate(['edit', this.data.id], {relativeTo: this.activatedRoute});
  }

  public async onDelete() {
    this.store.dispatch(tracksDeleteTrackAction({id: this.data.id}));
  }

}
