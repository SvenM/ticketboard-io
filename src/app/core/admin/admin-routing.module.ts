import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPage } from './admin.page';
import { AdminGuard } from './admin.guard';

const routes: Routes = [
  {
    path: '',
    component: AdminPage,
    canActivate: [AdminGuard]
  },
  {
    path: 'events',
    loadChildren: () => import('./events/event-list/event-list.module').then(m => m.EventListPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'events/new',
    loadChildren: () => import('./events/new-event/new-event.module').then(m => m.NewEventPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'events/edit/:eventId',
    loadChildren: () => import('./events/edit-event/edit-event.module').then(m => m.EditEventPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'promoters',
    loadChildren: () => import('./promoters/promoter-list/promoter-list.module').then(m => m.PromoterListPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'promoters/new',
    loadChildren: () => import('./promoters/new-promoter/new-promoter.module').then(m => m.NewPromoterPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'promoters/edit/:promoterId',
    loadChildren: () => import('./promoters/edit-promoter/edit-promoter.module').then(m => m.EditPromoterPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'tracks',
    loadChildren: () => import('./tracks/track-list/track-list.module').then(m => m.TrackListPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'tracks/new',
    loadChildren: () => import('./tracks/new-track/new-track.module').then(m => m.NewTrackPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'tracks/edit/:trackId',
    loadChildren: () => import('./tracks/edit-track/edit-track.module').then(m => m.EditTrackPageModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'business',
    loadChildren: () => import('./business/business.module').then( m => m.BusinessPageModule),
    canActivate: [AdminGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPageRoutingModule { }
