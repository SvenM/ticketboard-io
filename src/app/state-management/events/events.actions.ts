import { createAction, props } from '@ngrx/store';
import { Event } from '../../core/events/event.model';

export enum EventActions {
    GET_EVENT = '[Events] Get Event',
    UPDATE_EVENT = '[Events] Update Event',
    GET_EVENT_SUCCESSFUL = '[Events] Get Event Successful',
    UPDATE_EVENT_SUCCESSFUL = '[Events] Update Event Successful',
    DELETE_EVENT = '[Events] Delete Event',
    DELETE_EVENT_SUCCESSFUL = '[Events] Delete Event Successful',
}

export const eventsGetEventAction = createAction(EventActions.GET_EVENT, props<{id: string}>());
export const eventsGetEventSuccessfulAction = createAction(EventActions.GET_EVENT_SUCCESSFUL, props<{event: Partial<Event>}>());
export const eventsUpdateEventAction = createAction(EventActions.UPDATE_EVENT, props<{event: Partial<Event>}>());
export const eventsUpdateEventSuccessfulAction = createAction(EventActions.UPDATE_EVENT_SUCCESSFUL);
export const eventsDeleteEventAction = createAction(EventActions.DELETE_EVENT, props<{id: string}>());
export const eventsDeleteEventsSuccessfulAction = createAction(EventActions.DELETE_EVENT_SUCCESSFUL);

