import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EventService } from '../../core/events/event.service';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import {
    eventsDeleteEventAction, eventsDeleteEventsSuccessfulAction,
    eventsGetEventAction,
    eventsGetEventSuccessfulAction,
    eventsUpdateEventAction,
    eventsUpdateEventSuccessfulAction
} from './events.actions';

@Injectable()
export class EventsEffects {
    constructor(private actions$: Actions, private eventService: EventService) {
    }

    public getEvent$ = createEffect(() => this.actions$.pipe(
        ofType(eventsGetEventAction),
        switchMap(({id}) => {
            return this.eventService.getEvent(id);
        }),
        switchMap(event => {
            return of(eventsGetEventSuccessfulAction({event}));
        })
    ));

    public updateEvent$ = createEffect(() => this.actions$.pipe(
        ofType(eventsUpdateEventAction),
        switchMap(({event}) => {
            return this.eventService.updateEvent(event.id, event);
        }),
        switchMap(() => {
            return of(eventsUpdateEventSuccessfulAction());
        })
    ));

    public deleteEvent$ = createEffect(() => this.actions$.pipe(
        ofType(eventsDeleteEventAction),
        switchMap(({id}) => {
            return this.eventService.deleteEvent(id);
        }),
        switchMap(() => {
            return of(eventsDeleteEventsSuccessfulAction());
        })
    ));
}
