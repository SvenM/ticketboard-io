import { AppState } from '../../app.module';
import { createSelector } from '@ngrx/store';
import { EventsState } from './events.reducer';

export const selectEventsState = (state: AppState) => state.events;

export const selectEventsStateSelectedEvent = createSelector(
    selectEventsState,
    (state: EventsState) => state.selectedEvent
);

export const selectEventsStateLoading = createSelector(
    selectEventsState,
    (state: EventsState) => state.loading
);

