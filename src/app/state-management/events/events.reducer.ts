import { Action, createReducer, on } from '@ngrx/store';

import { Event } from '../../core/events/event.model';
import {
    eventsDeleteEventAction, eventsDeleteEventsSuccessfulAction,
    eventsGetEventAction,
    eventsGetEventSuccessfulAction,
    eventsUpdateEventAction,
    eventsUpdateEventSuccessfulAction
} from './events.actions';

export interface EventsState {
    loading: boolean;
    selectedEvent: Event;
}

export const initialEventsState: EventsState = {
    loading: false,
    selectedEvent: null,
};

const reducer = createReducer(
    initialEventsState,
    on(eventsUpdateEventAction, state => ({...state, loading: true})),
    on(eventsUpdateEventSuccessfulAction, state => ({...state, loading: false})),
    on(eventsGetEventAction, state => ({...state, loading: true, selectedEvent: null})),
    on(eventsGetEventSuccessfulAction, (state, {event}) => ({...state, loading: false, selectedEvent: event})),
    on(eventsDeleteEventAction, state => ({...state, loading: true})),
    on(eventsDeleteEventsSuccessfulAction, state => ({...state, loading: false})),
);

export function eventReducer(state: EventsState, action: Action) {
    return reducer(state, action);
}
