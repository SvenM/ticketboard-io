import { Action, createReducer, on } from '@ngrx/store';

import { Event } from '../../core/events/event.model';
import {
    promotersDeletePromoterAction, promotersDeletePromotersSuccessfulAction,
    promotersGetPromoterAction, promotersGetPromoterEventsAction, promotersGetPromoterEventsSuccessfulAction,
    promotersGetPromoterSuccessfulAction,
    promotersUpdatePromoterAction,
    promotersUpdatePromoterSuccessfulAction
} from './promoters.actions';
import { Promoter } from '../../core/promoters/promoter.model';

export interface PromotersState {
    loading: boolean;
    selectedPromoter: Promoter;
    events: Event[];
}

export const initialPromotersState: PromotersState = {
    loading: false,
    selectedPromoter: null,
    events: []
};

const reducer = createReducer(
    initialPromotersState,
    on(promotersUpdatePromoterAction, state => ({...state, loading: true})),
    on(promotersUpdatePromoterSuccessfulAction, state => ({...state, loading: false})),
    on(promotersGetPromoterAction, state => ({...state, loading: true, selectedPromoter: null})),
    on(promotersGetPromoterSuccessfulAction, (state, {promoter}) => ({...state, loading: false, selectedPromoter: promoter})),
    on(promotersDeletePromoterAction, state => ({...state, loading: true})),
    on(promotersDeletePromotersSuccessfulAction, state => ({...state, loading: false})),
    on(promotersGetPromoterEventsAction, state => ({...state, loading: true, events: []})),
    on(promotersGetPromoterEventsSuccessfulAction, (state, {events}) => ({...state, loading: false, events }))
);

export function promoterReducer(state: PromotersState, action: Action) {
    return reducer(state, action);
}
