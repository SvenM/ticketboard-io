import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PromoterService } from '../../core/promoters/promoter.service';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import {
    promotersDeletePromoterAction, promotersDeletePromotersSuccessfulAction,
    promotersGetPromoterAction, promotersGetPromoterEventsAction, promotersGetPromoterEventsSuccessfulAction,
    promotersGetPromoterSuccessfulAction,
    promotersUpdatePromoterAction,
    promotersUpdatePromoterSuccessfulAction
} from './promoters.actions';

@Injectable()
export class PromotersEffects {
    constructor(private actions$: Actions, private promoterService: PromoterService) {
    }

    public getPromoter$ = createEffect(() => this.actions$.pipe(
        ofType(promotersGetPromoterAction),
        switchMap(({id}) => {
            return this.promoterService.getPromoter(id);
        }),
        switchMap(promoter => {
            return of(promotersGetPromoterSuccessfulAction({promoter}));
        })
    ));

    public updatePromoter$ = createEffect(() => this.actions$.pipe(
        ofType(promotersUpdatePromoterAction),
        switchMap(({promoter}) => {
            return this.promoterService.updatePromoter(promoter.id, promoter);
        }),
        switchMap(() => {
            return of(promotersUpdatePromoterSuccessfulAction());
        })
    ));

    public deletePromoter$ = createEffect(() => this.actions$.pipe(
        ofType(promotersDeletePromoterAction),
        switchMap(({id}) => {
            return this.promoterService.deletePromoter(id);
        }),
        switchMap(() => {
            return of(promotersDeletePromotersSuccessfulAction());
        })
    ));

    public getPromoterEvents$ = createEffect(() => this.actions$.pipe(
        ofType(promotersGetPromoterEventsAction),
        switchMap(({id}) => {
            return this.promoterService.getPromoterEvents(id);
        }),
        switchMap(events => {
            return of(promotersGetPromoterEventsSuccessfulAction({events}));
        })
    ));
}
