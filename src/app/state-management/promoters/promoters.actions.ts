import { createAction, props } from '@ngrx/store';
import { Event } from '../../core/events/event.model';
import { Promoter } from '../../core/promoters/promoter.model';

export enum PromoterActions {
    GET_PROMOTER = '[Promoters] Get Promoter',
    UPDATE_PROMOTER = '[Promoters] Update Promoter',
    GET_PROMOTER_SUCCESSFUL = '[Promoters] Get Promoter Successful',
    UPDATE_PROMOTER_SUCCESSFUL = '[Promoters] Update Promoter Successful',
    DELETE_PROMOTER = '[Promoters] Delete Promoter',
    DELETE_PROMOTER_SUCCESSFUL = '[Promoters] Delete Promoter Successful',
    GET_PROMOTER_EVENTS = '[Promoters] Get Promoter Events',
    GET_PROMOTER_EVENTS_SUCCESSFUL = '[Promoters] Get Promoter Events Successful'
}

export const promotersGetPromoterAction = createAction(PromoterActions.GET_PROMOTER, props<{id: string}>());
export const promotersGetPromoterSuccessfulAction = createAction(PromoterActions.GET_PROMOTER_SUCCESSFUL, props<{promoter: Partial<Promoter>}>());
export const promotersUpdatePromoterAction = createAction(PromoterActions.UPDATE_PROMOTER, props<{promoter: Partial<Promoter>}>());
export const promotersUpdatePromoterSuccessfulAction = createAction(PromoterActions.UPDATE_PROMOTER_SUCCESSFUL);
export const promotersDeletePromoterAction = createAction(PromoterActions.DELETE_PROMOTER, props<{id: string}>());
export const promotersDeletePromotersSuccessfulAction = createAction(PromoterActions.DELETE_PROMOTER_SUCCESSFUL);
export const promotersGetPromoterEventsAction = createAction(PromoterActions.GET_PROMOTER_EVENTS, props<{id: string}>());
export const promotersGetPromoterEventsSuccessfulAction = createAction(PromoterActions.GET_PROMOTER_EVENTS_SUCCESSFUL, props<{events: Partial<Event>[]}>());

