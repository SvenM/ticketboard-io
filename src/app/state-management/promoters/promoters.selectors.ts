import { AppState } from '../../app.module';
import { createSelector } from '@ngrx/store';
import { PromotersState } from './promoters.reducer';

export const selectPromotersState = (state: AppState) => state.promoters;

export const selectPromotersStateSelectedPromoter = createSelector(
    selectPromotersState,
    (state: PromotersState) => state.selectedPromoter
);

export const selectPromotersStateLoading = createSelector(
    selectPromotersState,
    (state: PromotersState) => state.loading
);

export const selectPromotersStateEvents = createSelector(
    selectPromotersState,
    (state: PromotersState) => state.events
);
