import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TrackService } from '../../core/tracks/track.service';
import {
    tracksDeleteTrackAction, tracksDeleteTracksSuccessfulAction,
    tracksGetTrackAction, tracksGetTrackEventsAction, tracksGetTrackEventsSuccessfulAction,
    tracksGetTrackSuccessfulAction,
    tracksUpdateTrackAction,
    tracksUpdateTrackSuccessfulAction
} from './tracks.actions';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class TracksEffects {
    constructor(private actions$: Actions, private trackService: TrackService) {
    }

    public getTrack$ = createEffect(() => this.actions$.pipe(
        ofType(tracksGetTrackAction),
        switchMap(({id}) => {
            return this.trackService.getTrack(id);
        }),
        switchMap(track => {
            return of(tracksGetTrackSuccessfulAction({track}));
        })
    ));

    public updateTrack$ = createEffect(() => this.actions$.pipe(
        ofType(tracksUpdateTrackAction),
        switchMap(({track}) => {
            return this.trackService.updateTrack(track.id, track);
        }),
        switchMap(() => {
            return of(tracksUpdateTrackSuccessfulAction());
        })
    ));

    public deleteTrack$ = createEffect(() => this.actions$.pipe(
        ofType(tracksDeleteTrackAction),
        switchMap(({id}) => {
            return this.trackService.deleteTrack(id);
        }),
        switchMap(() => {
            return of(tracksDeleteTracksSuccessfulAction());
        })
    ));

    public getTrackEvents$ = createEffect(() => this.actions$.pipe(
        ofType(tracksGetTrackEventsAction),
        switchMap(({id}) => {
            return this.trackService.getTrackEvents(id);
        }),
        switchMap(events => {
            return of(tracksGetTrackEventsSuccessfulAction({events}));
        })
    ));
}
