import { AppState } from '../../app.module';
import { createSelector } from '@ngrx/store';
import { TracksState } from './tracks.reducer';

export const selectTracksState = (state: AppState) => state.tracks;

export const selectTracksStateSelectedTrack = createSelector(
    selectTracksState,
    (state: TracksState) => state.selectedTrack
);

export const selectTracksStateLoading = createSelector(
    selectTracksState,
    (state: TracksState) => state.loading
);

export const selectTracksStateEvents = createSelector(
    selectTracksState,
    (state: TracksState) => state.events
);
