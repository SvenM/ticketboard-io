import { createAction, props } from '@ngrx/store';
import { Track } from '../../core/tracks/track.model';
import { Event } from '../../core/events/event.model';

export enum TracksActions {
    GET_TRACK = '[Tracks] Get Track',
    UPDATE_TRACK = '[Tracks] Update Track',
    GET_TRACK_SUCCESSFUL = '[Tracks] Get Track Successful',
    UPDATE_TRACK_SUCCESSFUL = '[Tracks] Update Track Successful',
    DELETE_TRACK = '[Tracks] Delete Track',
    DELETE_TRACK_SUCCESSFUL = '[Tracks] Delete Track Successful',
    GET_TRACK_EVENTS = '[Tracks] Get Track Events',
    GET_TRACK_EVENTS_SUCCESSFUL = '[Tracks] Get Track Events Successful'
}

export const tracksGetTrackAction = createAction(TracksActions.GET_TRACK, props<{id: string}>());
export const tracksGetTrackSuccessfulAction = createAction(TracksActions.GET_TRACK_SUCCESSFUL, props<{track: Partial<Track>}>());
export const tracksUpdateTrackAction = createAction(TracksActions.UPDATE_TRACK, props<{track: Partial<Track>}>());
export const tracksUpdateTrackSuccessfulAction = createAction(TracksActions.UPDATE_TRACK_SUCCESSFUL);
export const tracksDeleteTrackAction = createAction(TracksActions.DELETE_TRACK, props<{id: string}>());
export const tracksDeleteTracksSuccessfulAction = createAction(TracksActions.DELETE_TRACK_SUCCESSFUL);
export const tracksGetTrackEventsAction = createAction(TracksActions.GET_TRACK_EVENTS, props<{id: string}>());
export const tracksGetTrackEventsSuccessfulAction = createAction(TracksActions.GET_TRACK_EVENTS_SUCCESSFUL, props<{events: Partial<Event>[]}>());

