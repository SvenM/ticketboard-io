import { Track } from '../../core/tracks/track.model';
import { Action, createReducer, on } from '@ngrx/store';
import {
    tracksDeleteTrackAction, tracksDeleteTracksSuccessfulAction,
    tracksGetTrackAction, tracksGetTrackEventsAction, tracksGetTrackEventsSuccessfulAction,
    tracksGetTrackSuccessfulAction,
    tracksUpdateTrackAction,
    tracksUpdateTrackSuccessfulAction
} from './tracks.actions';
import { Event } from '../../core/events/event.model';

export interface TracksState {
    loading: boolean;
    selectedTrack: Track;
    events: Event[];
}

export const initialTracksState: TracksState = {
    loading: false,
    selectedTrack: null,
    events: []
};

const reducer = createReducer(
    initialTracksState,
    on(tracksUpdateTrackAction, state => ({...state, loading: true})),
    on(tracksUpdateTrackSuccessfulAction, state => ({...state, loading: false})),
    on(tracksGetTrackAction, state => ({...state, loading: true, selectedTrack: null})),
    on(tracksGetTrackSuccessfulAction, (state, {track}) => ({...state, loading: false, selectedTrack: track})),
    on(tracksDeleteTrackAction, state => ({...state, loading: true})),
    on(tracksDeleteTracksSuccessfulAction, state => ({...state, loading: false})),
    on(tracksGetTrackEventsAction, state => ({...state, loading: true, events: []})),
    on(tracksGetTrackEventsSuccessfulAction, (state, {events}) => ({...state, loading: false, events }))
);

export function tracksReducer(state: TracksState, action: Action) {
    return reducer(state, action);
}
