import { Action, createReducer, on } from '@ngrx/store';

import {
    eventsFilterResetAllFiltersAction,
    eventsFilterSetAllFiltersAction,
} from './events-filter.actions';
import { FilterObject } from '../filter-object.model';

export enum BikeType {
    BIKE = 'bike',
    SUPERMOTO = 'supermoto',
    PITBIKE = 'pitbike',
    ROLLER = 'roller',
    ENDURO = 'enduro'
}

export enum TrainingType {
    RACE = 'race',
    SECURITY = 'security',
    MOTO_GP = 'moto_gp',
    WSBK = 'wsbk',
    IDM = 'idm',
    DLC = 'dlc',
    RLC = 'rlc',
    ALPE_ADRIA = 'alpe_adria',
    PRO_STK = 'pro_stk'
}

export interface EventsFilterState {
    hasFilters: boolean,
    filters: {
        [filterName: string]: FilterObject
    }
}

export const initialEventsFilterState: EventsFilterState = {
    hasFilters: false,
    filters: {}
};

const reducer = createReducer(
    initialEventsFilterState,
    on(eventsFilterResetAllFiltersAction, _ => ({
        ...initialEventsFilterState
    })),
    on(eventsFilterSetAllFiltersAction, (state, {filters}) => ({...state, hasFilters: true, filters: {...state.filters, ...filters}}))
);

export function eventsFilterReducer(state: EventsFilterState, action: Action) {
    return reducer(state, action);
}
