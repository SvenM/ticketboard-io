import { createAction, props } from '@ngrx/store';

import { FilterObject } from '../filter-object.model';

export enum EventsFilterActionType {
    RESET_ALL_FILTERS = '[Events Filter] Reset All',
    SET_ALL_FILTERS = '[Events Filter] Set All Filters',
}

export const eventsFilterResetAllFiltersAction = createAction(EventsFilterActionType.RESET_ALL_FILTERS);
export const eventsFilterSetAllFiltersAction = createAction(EventsFilterActionType.SET_ALL_FILTERS, props<{filters: { [filterName: string]: FilterObject }}>());
