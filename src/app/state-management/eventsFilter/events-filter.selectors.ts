import { createSelector } from '@ngrx/store';
import { AppState } from '../../app.module';
import { EventsFilterState } from './events-filter.reducer';

export const selectEventsFilterState = (state: AppState) => state.eventsFilter;

export const selectEventFiltersFilters = createSelector(
    selectEventsFilterState,
    (state: EventsFilterState) => Object.values(state.filters)
);

export const selectEventsFilterHasFilter = createSelector(
    selectEventsFilterState,
    (state: EventsFilterState) => state.hasFilters
);
