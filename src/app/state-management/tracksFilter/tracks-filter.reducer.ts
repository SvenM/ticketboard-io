import { FilterObject } from '../filter-object.model';
import { Action, createReducer, on } from '@ngrx/store';
import { tracksFilterResetAllFiltersAction, tracksFilterSetAllFiltersAction } from './tracks-filter.actions';

export interface TracksFilterState {
    hasFilters: boolean,
    filters: {
        [filterName: string]: FilterObject
    }
}

export const initialTracksFilterState: TracksFilterState = {
    hasFilters: false,
    filters: {}
};

const reducer = createReducer(
    initialTracksFilterState,
    on(tracksFilterResetAllFiltersAction, _ => ({
        hasFilters: false,
        filters: {}
    })),
    on(tracksFilterSetAllFiltersAction, (state, {filters}) => ({...state, hasFilters: true, filters: {...state.filters, ...filters}}))
)

export function tracksFilterReducer(state: TracksFilterState, action: Action) {
    return reducer(state, action);
}
