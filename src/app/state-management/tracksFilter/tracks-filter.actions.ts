import { createAction, props } from '@ngrx/store';
import { FilterObject } from '../filter-object.model';
import { CountryCodes } from '../../shared/models/filter-select.model';

export enum TracksFilterActionsType {
    RESET_ALL_FILTERS = '[Tracks Filter] Reset All',
    SET_ALL_FILTERS = '[Tracks Filter] Set All Filters',
    SET_COUNTRY = '[Tracks Filter] Set Country'
}

export const tracksFilterResetAllFiltersAction = createAction(TracksFilterActionsType.RESET_ALL_FILTERS);
export const tracksFilterSetAllFiltersAction = createAction(TracksFilterActionsType.SET_ALL_FILTERS, props<{filters: { [filterName: string]: FilterObject}}>());
