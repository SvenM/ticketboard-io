import { AppState } from '../../app.module';
import { createSelector } from '@ngrx/store';
import { TracksFilterState } from './tracks-filter.reducer';

export const selectTracksFilterState = (state: AppState) => state.tracksFilter;

export const selectTracksFiltersFilters = createSelector(
    selectTracksFilterState,
    (state: TracksFilterState) => Object.values(state.filters)
);

export const selectTracksFilterHasFilter = createSelector(
    selectTracksFilterState,
    (state: TracksFilterState) => state.hasFilters
);
