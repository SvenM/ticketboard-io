import WhereFilterOp = firebase.firestore.WhereFilterOp;

export interface FilterObject {
    name: string;
    operation: WhereFilterOp;
    value: any;
}
