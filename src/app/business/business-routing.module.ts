import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessPage } from './business.page';
import { BusinessAuthGuard } from './business-auth/business-auth.guard';

const routes: Routes = [
  {
    path: '',
    component: BusinessPage
  },
  {
    path: 'business-auth',
    loadChildren: () => import('./business-auth/business-auth.module').then( m => m.BusinessAuthPageModule)
  },
  {
    path: 'core',
    loadChildren: () => import('./core/core.module').then( m => m.CorePageModule),
    canLoad: [BusinessAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessPageRoutingModule {}
