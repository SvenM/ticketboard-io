export interface BusinessProfile {
    id: string;
    promoterId: string;
    isBusinessUser: boolean;
    isApproved: boolean;
    email: string;
}
