import { Injectable } from '@angular/core';
import { Auth2Service } from '../auth/auth2.service';
import { map, switchMap, take } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Observable, of } from 'rxjs';
import { BusinessAuthService } from './business-auth/business-auth.service';
import * as firebase from 'firebase';
import { BusinessProfile } from './business-profile';
import { PromoterService } from '../core/promoters/promoter.service';
import { Promoter } from '../core/promoters/promoter.model';

@Injectable({
    providedIn: 'root'
})
export class BusinessService {

    private BUSINESS_USER_COLLECTION = 'businessUsers';

    constructor(
        private authService: Auth2Service,
        private db: AngularFirestore,
        private businessAuthService: BusinessAuthService,
        private promoterService: PromoterService
    ) {
    }

    public getAllBusinessAccounts() {
        return this.authService.isUserAdmin().pipe(
            switchMap(isAdmin => {
                if (isAdmin) {
                    return this.db.collection(this.BUSINESS_USER_COLLECTION).valueChanges({idField: 'id'});
                }

                return of(null);
            })
        );
    }

    public getPromoterId() {
        return this.businessAuthService.businessUser.pipe(
            take(1),
            switchMap((user: firebase.User) => {
                return this.db.collection(this.BUSINESS_USER_COLLECTION).doc<BusinessProfile>(user.uid).get().pipe(map(ref => {
                    return ref.data().promoterId;
                }));
            })
        );
    }

    public getPromoterDoc(): Observable<Partial<Promoter>> {
        return this.getPromoterId().pipe(switchMap(id => {
            return from(this.promoterService.getPromoter(id));
        }));
    }

    public approveAccount(accountId: string, value: boolean) {
        this.authService.isUserAdmin().pipe(take(1)).subscribe(isAdmin => {
            if (isAdmin) {
                this.db
                    .collection(this.BUSINESS_USER_COLLECTION)
                    .doc(accountId)
                    .update({isApproved: value});
            }
        });
    }

    public assignPromoterId(accountId: string, promoterId: string): Promise<void> {
        return this.authService.isUserAdmin().pipe(
            take(1),
            switchMap(isAdmin => {
                if (isAdmin) {
                    return of(this.db.collection(this.BUSINESS_USER_COLLECTION).doc(accountId).update({promoterId}));
                }
                return of(null);
            })
        ).toPromise();
    }

    public removePromoterId(accountId: string): Promise<void> {
        return this.authService.isUserAdmin().pipe(
            take(1),
            switchMap(isAdmin => {
                if (isAdmin) {
                    return of(this.db.collection(this.BUSINESS_USER_COLLECTION).doc(accountId).update({promoterId: null}));
                }
                return of(null);
            })
        ).toPromise();
    }
}
