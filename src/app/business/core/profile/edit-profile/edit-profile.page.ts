import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { PromoterService } from '../../../../core/promoters/promoter.service';
import { SharedService } from '../../../../shared/shared.service';
import { DetailPageComponent } from '../../../../shared/components/classes/detail-page.component';

@Component({
    selector: 'tb-edit-profile',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage extends DetailPageComponent implements OnInit {

    public form: FormGroup;
    public isLoading = false;


    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private promoterService: PromoterService,
        private loaderCtrl: LoadingController,
        private sharedService: SharedService,
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {

        const id = this.route.snapshot.params.promoterId;

        if (!id) {
            this.navigateBack();
            return;
        }
        this.isLoading = true;
        const promoter = await this.promoterService.getPromoter(id).catch(error => {
            this.showAlert({message: 'Profile could not be fetched. Please try again later.'});
            return null;
        });

        if (!promoter) {
            await this.showAlert({message: 'Profile could not be fetched. Please try again later.'});
            return;
        }
        this.form = new FormGroup({
            name: new FormControl(promoter.name, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            homepage: new FormControl(promoter.homepage, {
                updateOn: 'change',
            }),
            contact: new FormControl(promoter.contact, {
                updateOn: 'change',
            }),
            address: new FormControl(promoter.address, {
                updateOn: 'change',
            }),
            zip: new FormControl(promoter.zip, {
                updateOn: 'change',
            }),
            city: new FormControl(promoter.city, {
                updateOn: 'change',
            }),
            telephone: new FormControl(promoter.telephone, {
                updateOn: 'change',
            }),
            email: new FormControl(promoter.email, {
                updateOn: 'change',
            }),
            logoUrl: new FormControl(promoter.logoUrl)
        });
        this.isLoading = false;
    }

    public async onUpdatePromoter() {
        const id = this.route.snapshot.params.promoterId;

        await this.promoterService.updatePromoter(id, {
            name: this.form.value.name,
            homepage: this.form.value.homepage ? this.form.value.homepage : null,
            contact: this.form.value.contact ? this.form.value.contact : null,
            address: this.form.value.address ? this.form.value.address : null,
            zip: this.form.value.zip ? this.form.value.zip : null,
            city: this.form.value.city ? this.form.value.city : null,
            telephone: this.form.value.telephone ? this.form.value.telephone : null,
            email: this.form.value.email ? this.form.value.email : null,
            logoUrl: this.form.value.logoUrl ? this.form.value.logoUrl : null
        });

        this.navigateBack();
    }

    public async onImagePicked(imageData: string | File) {
        await this.sharedService.onImagePicked(imageData, this.form, 'logoUrl');
    }

}
