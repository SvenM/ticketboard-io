import { Component, OnInit } from '@angular/core';
import { BusinessAuthService } from '../../business-auth/business-auth.service';
import { from, Observable, of } from 'rxjs';
import { Promoter } from '../../../core/promoters/promoter.model';
import { switchMap, take, tap } from 'rxjs/operators';
import { BusinessService } from '../../business.service';
import { PromoterService } from '../../../core/promoters/promoter.service';

@Component({
    selector: 'tb-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    public isApproved: boolean;
    public isLoading = false;
    public promoter: Promoter;

    constructor(
        private businessAuthService: BusinessAuthService,
        private businessService: BusinessService,
        private promoterService: PromoterService
    ) {
    }

    public ngOnInit() {
        this.init();
    }

    public ionViewWillEnter() {
        this.init();
    }

    private init() {
        this.isLoading = true;

        this.businessAuthService.isApproved().pipe(
            take(1),
            tap(approved => {
                this.isApproved = approved;
            }),
            switchMap(approved => {
                if (approved) {
                    return this.businessService.getPromoterId().pipe(
                        take(1),
                        switchMap(promoterId => {
                            if (!promoterId) {
                                return of(null);
                            }

                            return from(this.promoterService.getPromoter(promoterId));
                        }));
                }
            }),
            tap(promoter => {
                this.promoter = promoter;
                this.isLoading = false;
            })
        ).subscribe();
    }
}
