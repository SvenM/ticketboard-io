import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorePage } from './core.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/business/core/profile',
    pathMatch: 'full'
  },
  {
    path: '',
    component: CorePage,
    children: [
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
      },
      {
        path: 'businessEvents',
        loadChildren: () => import('./events/businessEvents.module').then(m => m.BusinessEventPageModule)
      },
      {
        path: 'messenger',
        loadChildren: () => import('./messenger/messenger.module').then( m => m.MessengerPageModule)
      },
      {
        path: 'analytics',
        loadChildren: () => import('./analytics/analytics.module').then( m => m.AnalyticsPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorePageRoutingModule {}
