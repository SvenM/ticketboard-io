import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessEventsPage } from './businessEvents.page';
import { EventDetailPage } from '../../../core/events/event-detail/event-detail.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessEventsPage
  },
  {
    path: 'event/:id',
    component: EventDetailPage
  },
  {
    path: 'business-add-event',
    loadChildren: () => import('./business-add-event/business-add-event.module').then( m => m.BusinessAddEventPageModule)
  },
  {
    path: 'edit/:eventId',
    loadChildren: () => import('./edit-business-event/edit-business-event.module').then( m => m.EditBusinessEventPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventsPageRoutingModule {}
