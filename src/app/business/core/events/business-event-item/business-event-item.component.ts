import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event } from '../../../../core/events/event.model';
import { Router } from '@angular/router';
import { EventService } from '../../../../core/events/event.service';

@Component({
    selector: 'tb-business-event-item',
    templateUrl: './business-event-item.component.html',
    styleUrls: ['./business-event-item.component.scss'],
})
export class BusinessEventItemComponent implements OnInit {

    @Input() data: Event;
    @Output() delete: EventEmitter<void> = new EventEmitter<void>();

    constructor(private router: Router, private eventService: EventService) {
    }

    public ngOnInit() {
    }

    public onEdit() {
        this.router.navigate(['/', 'business', 'core', 'businessEvents', 'edit', this.data.id]);
    }

    public onDelete() {
        this.eventService.deleteEvent(this.data.id).then(() => {
            this.delete.emit();
        });
    }
}
