import { TestBed } from '@angular/core/testing';

import { BusinessEventsService } from './business-events.service';

describe('BusinessEventsService', () => {
  let service: BusinessEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessEventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
