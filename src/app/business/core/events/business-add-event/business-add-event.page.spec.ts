import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusinessAddEventPage } from './business-add-event.page';

describe('BusinessAddEventPage', () => {
  let component: BusinessAddEventPage;
  let fixture: ComponentFixture<BusinessAddEventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAddEventPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusinessAddEventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
