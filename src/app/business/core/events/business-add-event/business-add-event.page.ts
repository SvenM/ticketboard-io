import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EventService } from '../../../../core/events/event.service';
import { SharedService } from '../../../../shared/shared.service';
import { BusinessService } from '../../../business.service';
import { take } from 'rxjs/operators';
import { DetailPageComponent } from '../../../../shared/components/classes/detail-page.component';

@Component({
    selector: 'tb-business-add-event',
    templateUrl: './business-add-event.page.html',
    styleUrls: ['./business-add-event.page.scss'],
})
export class BusinessAddEventPage extends DetailPageComponent implements OnInit {
    public form: FormGroup;
    isLoading = false;

    constructor(
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        private router: Router,
        private eventService: EventService,
        private sharedService: SharedService,
        private businessService: BusinessService,
    ) {
        super(navCtrl, alertCtrl);
    }

    public ngOnInit() {

        this.isLoading = true;

        this.businessService.getPromoterDoc().pipe(take(1)).subscribe(async promoter => {
            if (!promoter) {
                await this.showAlert({message: 'Error while loading this site. Please try again later.'});

                return;
            } else {
                this.form = new FormGroup({
                    promoter: new FormControl(promoter, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    dateFrom: new FormControl(null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    dateTo: new FormControl(null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    track: new FormControl(null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    price: new FormControl(null, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    catering: new FormControl(false, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    instructions: new FormControl(false, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    races: new FormControl(false, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    photoService: new FormControl(false, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    service: new FormControl(false, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                    timekeeping: new FormControl(false, {
                        updateOn: 'change',
                        validators: [Validators.required]
                    }),
                });

                this.isLoading = false;
            }
        });
    }

    public async onCreateEvent() {
        if (!this.form.valid) {
            return;
        }

        await this.eventService.addEvent({
            promoter: this.form.value.promoter,
            dateFrom: this.sharedService.createDateFrom(this.form.value.dateFrom),
            dateTo: this.sharedService.createDateTo(this.form.value.dateTo),
            track: this.form.value.track,
            price: this.form.value.price,
            country: this.form.value.track.country,
            trainingType: null,
            difficulty: null,
            catering: this.form.value.catering,
            instructions: this.form.value.instructions,
            races: this.form.value.races,
            photoService: this.form.value.photoService,
            service: this.form.value.service,
            timekeeping: this.form.value.timekeeping
        }).catch(() => {
            this.showAlert({message: 'Error while creating event. Please try again later.'});
        });

        this.form.reset();
        await this.router.navigate(['/business/core/businessEvents']);
    }
}
