import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessAddEventPage } from './business-add-event.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessAddEventPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessAddEventPageRoutingModule {}
