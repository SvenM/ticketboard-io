import { Component, OnInit } from '@angular/core';
import { BusinessEventsService } from './business-events.service';
import { Event } from '../../../core/events/event.model';

@Component({
    selector: 'tb-events',
    templateUrl: './businessEvents.page.html',
    styleUrls: ['./businessEvents.page.scss'],
})
export class BusinessEventsPage implements OnInit {


    constructor(
        public events$: BusinessEventsService,
    ) {
    }

    public ngOnInit() {
        this.events$.init();
    }

    public ionViewWillEnter() {
        this.events$.init();
    }

    public trackByEventId(_, event: Event) {
        return event.id;
    }

    public onItemWasDeleted() {
        this.events$.init();
    }

}
