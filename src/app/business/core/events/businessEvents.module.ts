import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventsPageRoutingModule } from './businessEvents-routing.module';

import { BusinessEventsPage } from './businessEvents.page';
import { SharedModule } from '../../../shared/shared.module';
import { BusinessEventItemComponent } from './business-event-item/business-event-item.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EventsPageRoutingModule,
        SharedModule
    ],
    declarations: [BusinessEventsPage, BusinessEventItemComponent],
    exports: [BusinessEventItemComponent]
})
export class BusinessEventPageModule {
}
