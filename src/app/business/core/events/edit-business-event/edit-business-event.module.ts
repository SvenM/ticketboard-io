import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditBusinessEventPageRoutingModule } from './edit-business-event-routing.module';

import { EditBusinessEventPage } from './edit-business-event.page';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EditBusinessEventPageRoutingModule,
        SharedModule,
        ReactiveFormsModule
    ],
  declarations: [EditBusinessEventPage]
})
export class EditBusinessEventPageModule {}
