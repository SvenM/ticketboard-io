import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditBusinessEventPage } from './edit-business-event.page';

const routes: Routes = [
  {
    path: '',
    component: EditBusinessEventPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditBusinessEventPageRoutingModule {}
