import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditBusinessEventPage } from './edit-business-event.page';

describe('EditBusinessEventPage', () => {
  let component: EditBusinessEventPage;
  let fixture: ComponentFixture<EditBusinessEventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBusinessEventPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditBusinessEventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
