import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { EventService } from '../../../../core/events/event.service';
import { SharedService } from '../../../../shared/shared.service';
import { Event } from '../../../../core/events/event.model';
import { DetailPageComponent } from '../../../../shared/components/classes/detail-page.component';

@Component({
    selector: 'tb-edit-business-event',
    templateUrl: './edit-business-event.page.html',
    styleUrls: ['./edit-business-event.page.scss'],
})
export class EditBusinessEventPage extends DetailPageComponent implements OnInit {

    form: FormGroup;
    isLoading = false;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private route: ActivatedRoute,
        private eventService: EventService,
        private loaderCtrl: LoadingController,
        private sharedService: SharedService,
    ) {
        super(navCtrl, alertCtrl);
    }

    public async ngOnInit() {
        const id = this.route.snapshot.params.id;

        if (!id) {
            await this.navigateBack();
            return;
        }

        this.isLoading = true;

        const event: Event = await this.eventService.getEvent(id).catch(() => {
            this.showAlert({ message: 'Event could not be fetched. Please try again later.'});
            return null;
        });

        if (!event?.dateFrom) {
            await this.showAlert({ message: 'Event could not be fetched. Please try again later.'});
            return;
        }

        this.form = new FormGroup({
            promoter: new FormControl(event.promoter, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            dateFrom: new FormControl(
                event.dateFrom ? new Date(event.dateFrom).toUTCString() : null,
                {
                    updateOn: 'change',
                    validators: [Validators.required]
                }
            ),
            dateTo: new FormControl(event.dateTo ? new Date(event.dateTo).toUTCString() : null,
                {
                    updateOn: 'change',
                    validators: [Validators.required]
                }
            ),
            track: new FormControl(event.track, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            price: new FormControl(event.price, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            catering: new FormControl(event.catering, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            instructions: new FormControl(event.instructions, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            races: new FormControl(event.races, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            photoService: new FormControl(event.photoService, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            service: new FormControl(event.service, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            timekeeping: new FormControl(event.timekeeping, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
        });
        this.isLoading = false;
    }

    public async onUpdateEvent() {
        if (!this.form.valid) {
            return;
        }

        const id = this.route.snapshot.params.id;

        await this.eventService.updateEvent(id, {
            promoter: this.form.value.promoter,
            dateFrom: this.form.value.dateFrom ? this.sharedService.createDateFrom(this.form.value.dateFrom) : null,
            dateTo: this.form.value.dateTo ? this.sharedService.createDateTo(this.form.value.dateTo) : null,
            track: this.form.value.track,
            price: this.form.value.price,
            country: this.form.value.track.country,
            trainingType: null,
            difficulty: null,
            catering: this.form.value.catering,
            instructions: this.form.value.instructions,
            races: this.form.value.races,
            photoService: this.form.value.photoService,
            service: this.form.value.service,
            timekeeping: this.form.value.timekeeping
        });

        await this.sharedService.showSpinner('Updating...');
        this.form.reset();
        await this.navigateBack();
    }

}
