import { Injectable, OnDestroy } from '@angular/core';
import { AngularFirestore, } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { Event } from '../../../core/events/event.model';
import { scan, take, takeUntil, tap } from 'rxjs/operators';
import { BusinessService } from '../../business.service';
import { AppState } from '../../../app.module';
import { Store } from '@ngrx/store';
import { InfiniteLoopService } from '../../../shared/services/infinite-loop.service';

@Injectable({
    providedIn: 'root'
})
export class BusinessEventsService extends InfiniteLoopService implements OnDestroy {

    private EVENTS_COLLECTION: string;
    private collectionSuffix = 'Events';

    public data: Observable<Partial<Event>[]>;
    public done: Observable<boolean> = this._done.asObservable();
    public loading: Observable<boolean> = this._loading.asObservable();

    private unsubscribe: Subject<void>;


    constructor(
        private db: AngularFirestore,
        private businessService: BusinessService,
    ) {
        super();
        this.unsubscribe = new Subject<any>();
    }

    public ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();

    }

    public init() {
        this.businessService.getPromoterId().pipe(
            take(1),
            tap(promoterId => {
                if (!promoterId) {
                    return;
                }

                this.query = this.db
                    .collection<Partial<Event>[]>(this.EVENTS_COLLECTION)
                    .ref
                    .limit(15)
                    .orderBy('dateFrom')
                    .where('promoter.id', '==', promoterId);

                const first = this.db.collection(this.EVENTS_COLLECTION, ref => this.query);

                this._data.next([]);
                this._done.next(false);
                this.mapAndUpdate(first, this.updateCallback);

                this.data = this._data.asObservable().pipe(scan((acc, val) => {
                    return acc.concat(val);
                }));
            })).subscribe();
    }

    public more() {
        const cursor = this.getCursor();

        const more = this.db.collection(this.EVENTS_COLLECTION, ref => this.query.startAfter(cursor));
        this.mapAndUpdate(more, this.updateCallback);
    }

    private updateCallback = snap => {
        const data = {
            id: snap.payload.doc.id,
            ...snap.payload.doc.data(),
            dateFrom: snap.payload.doc.data().dateFrom ? (snap.payload.doc.data().dateFrom as any).toDate() : null,
            dateTo: snap.payload.doc.data().dateTo ? (snap.payload.doc.data().dateTo as any).toDate() : null
        };
        const doc = snap.payload.doc;
        return {...data, doc};
    }

    private changeFilter() {
        this._data.next([]);
        this.init();
    }
}
