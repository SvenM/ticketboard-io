import { Injectable } from '@angular/core';
import { Router, Route, UrlSegment, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { BusinessAuthService } from './business-auth.service';
import { map, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BusinessAuthGuard implements CanLoad {

  constructor(private businessAuthService: BusinessAuthService, private router: Router) {
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.businessAuthService.businessUser.pipe(
        take(1),
        map(user => !!user),
        tap(isAuthenticated => {
          if (!isAuthenticated) {
            this.router.navigateByUrl('/business/business-auth');
          }
        }));
  }
}
