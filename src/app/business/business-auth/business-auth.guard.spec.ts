import { TestBed } from '@angular/core/testing';

import { BusinessAuthGuard } from './business-auth.guard';

describe('BusinessAuthGuard', () => {
  let guard: BusinessAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(BusinessAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
