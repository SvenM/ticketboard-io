import { Injectable, NgZone } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as firebase from 'firebase/app';
import { User } from 'firebase';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';
import { BusinessProfile } from '../business-profile';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
    providedIn: 'root'
})
export class BusinessAuthService {

    public businessUser: Observable<User>;
    private isLoading = false;

    constructor(
        private firebaseAuth: AngularFireAuth,
        private loadingCtrl: LoadingController,
        private ngZone: NgZone,
        private router: Router,
        private alertCtrl: AlertController,
        private db: AngularFirestore,
    ) {
        this.businessUser = firebaseAuth.authState;
    }

    public async signup(email: string, password: string): Promise<void> {
        const loadingEl = await this.showLoadingSpinner();
        loadingEl.present();

        const value = await firebase.auth()
            .createUserWithEmailAndPassword(email, password).catch(err => {
                this.isLoading = false;
                loadingEl.dismiss();
                this.showAlert(err.message);
                return null;
            });

        this.signUpSuccessful(value);
        this.isLoading = false;
        await loadingEl.dismiss();
        await this.router.navigateByUrl('/business/core');
        return value;
    }

    public async login(email: string, password: string): Promise<void> {
        const loadingEl = await this.showLoadingSpinner();
        await loadingEl.present();

        await firebase.auth()
            .signInWithEmailAndPassword(email, password)
            .catch(err => {
                this.isLoading = false;
                loadingEl.dismiss();
                this.showAlert(err.message);
                return null;
            });

        this.isLoading = false;
        await loadingEl.dismiss();
        await this.router.navigateByUrl('/business/core');
    }

    public logout(): Promise<void> {
        return firebase.auth().signOut();
    }

    public async recoverPassword(passwordResetEmail) {
        await firebase.auth()
            .sendPasswordResetEmail(passwordResetEmail)
            .catch((error) => {
                this.showAlert(error.message);
            });

        const message = 'A password reset email has been sent, please check your inbox';
        const alertEl = await this.alertCtrl.create({header: 'Check your inbox', message, buttons: ['Okay']});
        await alertEl.present();

    }

    public isBusinessUser(): Observable<boolean> {
        return this.businessUser.pipe(switchMap((user: firebase.User) => {
            if (user && user.uid) {
                return this.db
                    .collection('businessUsers')
                    .doc(user.uid)
                    .get()
                    .pipe(
                        map(userRef => userRef.get('isBusinessUser')),
                        catchError(err => of(false))
                    );
            }

            return of(false);
        }));
    }

    public isApproved(): Observable<boolean> {
        return this.businessUser.pipe(switchMap((user: firebase.User) => {
            if (user && user.uid) {
                return this.db
                    .collection('businessUsers')
                    .doc(user.uid)
                    .get()
                    .pipe(
                        map(userRef => userRef.get('isApproved')),
                        catchError(err => of(false))
                    );
            }

            return of(false);
        }));
    }

    private showLoadingSpinner() {
        this.isLoading = true;
        return this.loadingCtrl
            .create({keyboardClose: true, message: 'Logging in...'});
    }

    private signUpSuccessful(userData: firebase.auth.UserCredential) {
        this.db
            .collection('businessUsers')
            .doc(userData.user.uid)
            .get()
            .pipe(
                take(1),
                catchError(() => of(null))
            )
            .subscribe(userDoc => {
                if (userDoc) {
                    if (!userDoc.exists) {
                        this.db.collection('businessUsers').doc<BusinessProfile>(userData.user.uid).set({
                            id: userData.user.uid,
                            isApproved: false,
                            isBusinessUser: true,
                            promoterId: null,
                            email: userData.user.email
                        });
                    }
                }

                this.router.navigateByUrl('/business/core');
            });
    }

    private async showAlert(message: string) {
        const alertEl = await this.alertCtrl.create({header: 'Authentication failed', message, buttons: ['Okay']});
        alertEl.present();
    }
}
