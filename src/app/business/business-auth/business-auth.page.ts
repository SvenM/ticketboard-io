import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BusinessAuthService } from './business-auth.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'tb-business-auth',
    templateUrl: './business-auth.page.html',
    styleUrls: ['./business-auth.page.scss'],
})
export class BusinessAuthPage implements OnInit {

    public form: FormGroup;

    public isLoading = false;
    public isLogin = true;

    constructor(
        private businessAuthService: BusinessAuthService,
        private router: Router,
    ) {
    }

    public ngOnInit() {
        this.form = this.createLoginForm();
    }

    public async onSubmit() {
        if (!this.form.valid) {
            return;
        }
        const email = this.form.value.email;
        const password = this.form.value.password;

        await this.authenticate(email, password);
        this.form.reset();
    }

    public async authenticate(email, password) {
        if (this.isLogin) {
            await this.businessAuthService.login(email, password);
        } else {
            await this.businessAuthService.signup(email, password);
        }
    }

    public onPasswordForgotten() {
        this.router.navigateByUrl('/auth/password-forgotten');
    }

    public onSwitchAuthMode() {
        this.isLogin = !this.isLogin;
        if (this.isLogin) {
            this.form = this.createLoginForm();
        } else {
            this.form = this.createSignUpForm();
        }
    }

    private createSignUpForm() {
        return new FormGroup({
            email: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.email]
            }),
            password: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            })
        });
    }

    private createLoginForm() {
        return new FormGroup({
            email: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            password: new FormControl(null, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            })
        });
    }
}
