import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessAuthPageRoutingModule } from './business-auth-routing.module';

import { BusinessAuthPage } from './business-auth.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        BusinessAuthPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [BusinessAuthPage]
})
export class BusinessAuthPageModule {
}
