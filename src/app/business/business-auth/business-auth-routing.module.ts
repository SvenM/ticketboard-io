import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessAuthPage } from './business-auth.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessAuthPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessAuthPageRoutingModule {}
