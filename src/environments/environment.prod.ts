export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyBWx5Z6lA3rzntG7g1_3M3iZmnpyokYBRk',
    authDomain: 'ticketboard-prod.firebaseapp.com',
    databaseURL: 'https://ticketboard-prod.firebaseio.com',
    projectId: 'ticketboard-prod',
    storageBucket: 'ticketboard-prod.appspot.com',
    messagingSenderId: '205480597257',
    appId: '1:205480597257:web:920c776e5fefe6b78ba5d8',
    measurementId: 'G-P2HL7D9T78'
  }
};
