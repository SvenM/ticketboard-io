// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDqHo_41n9YWtGF74EqxFOlfmfARvhOJwo',
    authDomain: 'ticketboard-dev.firebaseapp.com',
    databaseURL: 'https://ticketboard-dev.firebaseio.com',
    projectId: 'ticketboard-dev',
    storageBucket: 'ticketboard-dev.appspot.com',
    messagingSenderId: '102607298683',
    appId: '1:102607298683:web:ce2d8e9d2396e1b61002de',
    measurementId: 'G-B43LTJ4TG4'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
